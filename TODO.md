# TODO

- IMPORTANT Add copy constructors and assignment operators !!!
- Note edit has to be implemented
- handle runtime error when creating new active session when active session already exists
- delete task
- delete note
- list note ids on task list, along with session ids
- note/note-ids exclude listing in task/session
- session by id
- note by task/session from note interface
- Add tests for app
- Add rest of tests for Model if possible
- Add tests for utils
- Allow listing sessions in some time period
- Show session by id
- Add `--pause` & `--unpause` to simple session app. along with separate apps
- Unlink from session level?
- Possible pomodoro integration (?)
- Rewrite Model module to insert `id` by default or to work with tables with no `id` field
- Add field in Task model to flag linked session(s)
- Add fiels in Session model to flag linked task(s)
- Refactor XD
- Option to get get Note(s) linked to Task & Session
- Tables
- Graphs (?)

# Additional TODOs

- CRUD api for web mode
- Add online mode - syncing DBs after each sun or only online DB
- add "Current Task" for default task id selection like current ongoing session
