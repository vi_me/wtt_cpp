#pragma once

#ifdef WTT_TEST
#include "gtest/gtest_prod.h"
#endif

#include <string>
#include <vector>
#include <memory>

#include "wtt_core/Model.hpp"
#include "wtt_core/Field.hpp"

// TODO: consider adding linking functionality here
class Note : public Model {

private:

    static bool table_created;
    static bool check_table();

protected:

    inline static const std::string name = "notes";

public:

    std::vector<std::shared_ptr<Field>> fields = {
        std::make_shared<Field>("id",                 "INTEGER PRIMARY KEY AUTOINCREMENT"),
        std::make_shared<Field>("text",               "TEXT"),
        std::make_shared<Field>("task_id",            "INTEGER", "tasks(id)"),
        std::make_shared<Field>("session_id",         "INTEGER", "sessions(id)"),
        std::make_shared<Field>("created_timestamp",  "INTEGER DEFAULT (strftime('\u0025s', 'now', 'localtime')) NOT NULL")
    };
    std::vector<std::string> constraints = {};

    Note();
    Note(const std::vector<SqlValPair>&);
    ~Note();

    // DB selectors
    static std::shared_ptr<Note> get(const std::string&);
    static std::shared_ptr<Note> get(const long&);  // get_new() that creates on the heap???
    static std::vector<std::shared_ptr<Note>> filter(const std::string& = "");

    // DB
    // void save() override;
    // void update() override;
    // void del() override;

    //
    // std::shared_ptr<Model>              init_new_as_model() const override;  NOTE: consider removing
    std::string                         get_name() const override;
    std::shared_ptr<Field>              get_field(const std::string&) const override;
    std::vector<std::shared_ptr<Field>> get_fields() const override;
    std::vector<std::string>            get_constraints() const override;
    void                                set_value(const std::string&, const std::string&) override;
    void                                set_value(const SqlValPair&) override;

    // static void create_table();
    static const bool get_table_created() { return Note::table_created; }
    friend std::ostream& operator<<(std::ostream& os, const Note& note);

    // TESTING
    #ifdef WTT_TEST
    FRIEND_TEST(NoteClassTest, constructor_test);
    FRIEND_TEST(NoteClassTest, constructor_test_missing_table);
    FRIEND_TEST(NoteClassTest, constructor_with_values_test);
    FRIEND_TEST(NoteClassTest, constructor_with_values_test_missing_table);
    FRIEND_TEST(TaskClassTest, constructor_test);
    #endif
};
