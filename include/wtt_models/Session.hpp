#pragma once

#ifdef WTT_TEST
#include "gtest/gtest_prod.h"
#endif

#include <string>
#include <vector>
#include <memory>

#include "conf.h"
#include "wtt_core/Model.hpp"
#include "wtt_core/Field.hpp"


class Session : public Model {

private:

    static bool table_created;
    static bool check_table();

protected:

    inline static const std::string name = "sessions";


public:

    std::vector<std::shared_ptr<Field>> fields = std::vector<std::shared_ptr<Field>>({
        std::make_shared<Field>("id",                       "INTEGER PRIMARY KEY AUTOINCREMENT"),
        std::make_shared<Field>("start_timestamp",          "INTEGER DEFAULT (strftime('\u0025s', 'now')) NOT NULL"),
        std::make_shared<Field>("end_timestamp",            "INTEGER DEFAULT NULL"),
        std::make_shared<Field>("active",                   "BOOLEAN DEFAULT TRUE"),
        std::make_shared<Field>("pause",                    "REAL DEFAULT 0"),
        std::make_shared<Field>("paused",                   "BOOLEAN DEFAULT FALSE"),
        std::make_shared<Field>("pause_start_timestamp",    "INTEGER DEFAULT NULL")
    });
    std::vector<std::string> constraints = std::vector<std::string>({
        "CHECK (end_timestamp > start_timestamp)"
    });

    Session();
    Session(const std::vector<SqlValPair>&);
    ~Session();

    //DB selectors
    static std::shared_ptr<Session>              get(const long&);
    static std::shared_ptr<Session>              get(const std::string&);
    static std::vector<std::shared_ptr<Session>> filter(const std::string& = "");
    static bool                                  exists(const long&);
    static bool                                  exists(const std::string&);

    // DB
    // void save() override;
    // void update() override;
    // void del() override;

    // Model overrides
    // std::shared_ptr<Model>              init_new_as_model() const override;  NOTE: consider removing
    std::string                         get_name() const override;
    std::shared_ptr<Field>              get_field(const std::string&) const override;
    std::vector<std::shared_ptr<Field>> get_fields() const override;
    std::vector<std::string>            get_constraints() const override;
    void                                set_value(const std::string&, const std::string&) override;
    void                                set_value(const SqlValPair&) override;

    // void create_table();  NOTE: consider

    // Utility methods
    static                          std::shared_ptr<Session> start(); // in future might return a Session obj
    static                          std::shared_ptr<Session> start_at(const std::string&);
    MOCKABLE void                   stop();
    void                            stop_at(const std::string& = "");  // TODO: pack into default start
    static std::shared_ptr<Session> create_at(const std::string&, const std::string& = "");  // TODO: pack into default stop
    static std::shared_ptr<Session> get_active_session();
    void                            pause(const std::string& = "");
    void                            unpause(const std::string& = "");
    long                            get_duration();
    static const bool               get_table_created() { return Session::table_created; }
    friend std::ostream&            operator<<(std::ostream& os, const Session& session);

    // TESTING
    #ifdef WTT_TEST
    FRIEND_TEST(SessionClassTest, constructor_test);
    FRIEND_TEST(SessionClassTest, constructor_test_missing_table);
    FRIEND_TEST(SessionClassTest, constructor_with_values_test);
    FRIEND_TEST(SessionClassTest, constructor_with_values_test_missing_table);
    #endif
};
