#pragma once

#ifdef WTT_TEST
#include "gtest/gtest_prod.h"
#endif

#include <string>
#include <vector>
#include <memory>

#include "wtt_core/Model.hpp"
#include "wtt_core/Field.hpp"


class Task : public Model {

private:

    static bool table_created;
    static bool check_table();

protected:

    inline static const std::string name = "tasks";

public:

    // enum STATUS_MAPPING {
    //     CREATED,
    //     TODO,
    //     IN_PROGRESS,
    //     DONE,
    //     REJECTED
    // };

    std::vector<std::shared_ptr<Field>> fields = std::vector<std::shared_ptr<Field>>({
        std::make_shared<Field>("id",                 "INTEGER PRIMARY KEY AUTOINCREMENT"),
        std::make_shared<Field>("summary",            "TEXT"),
        std::make_shared<Field>("status",             "TEXT DEFAULT 'TODO' NOT NULL"),
        std::make_shared<Field>("description",        "TEXT DEFAULT ''"),
        std::make_shared<Field>("created_timestamp",  "INTEGER DEFAULT (strftime('\u0025s', 'now', 'localtime')) NOT NULL")
    });
    std::vector<std::string> constraints = {
        "CHECK (status IN ('TODO', 'IN PROGRESS', 'DONE'))"
    };

    Task();
    Task(const std::vector<SqlValPair>&);
    ~Task();

    // DB selectors
    static std::shared_ptr<Task>              get(const long&);  // get_new() that creates on the heap???
    static std::shared_ptr<Task>              get(const std::string&);
    static std::vector<std::shared_ptr<Task>> filter(const std::string& = "");
    static bool                               exists(const long&);
    static bool                               exists(const std::string&);

    // DB
    // void save() override;
    // void update() override;
    // void del() override;

    //
    // std::shared_ptr<Model> init_new_as_model() const override;  NOTE: consider removing
    std::string get_name() const override; 
    std::shared_ptr<Field> get_field(const std::string&) const override;
    std::vector<std::shared_ptr<Field>> get_fields() const override;
    std::vector<std::string> get_constraints() const override;
    void set_value(const std::string&, const std::string&) override;
    void set_value(const SqlValPair&) override;

    static void create_table();

    static const bool get_table_created() { return Task::table_created; }
    friend std::ostream& operator<<(std::ostream& os, const Task& task);

    // TESTING
    #ifdef WTT_TEST
    FRIEND_TEST(TaskClassTest, constructor_test);
    FRIEND_TEST(TaskClassTest, constructor_test_missing_table);
    FRIEND_TEST(TaskClassTest, constructor_with_values_test);
    FRIEND_TEST(TaskClassTest, constructor_with_values_test_missing_table);
    #endif
};
