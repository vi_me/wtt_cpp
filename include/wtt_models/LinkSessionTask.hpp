#pragma once

#ifdef WTT_TEST
#include "gtest/gtest_prod.h"
#endif

#include <memory>
#include <string>
#include <vector>

#include "wtt_core/Model.hpp"
#include "wtt_core/Field.hpp"


class LinkSessionTask : public Model {

private:

    static bool table_created;
    static bool check_table();

protected:

    inline static const std::string name = "sessions_tasks";

public:

    std::vector<std::shared_ptr<Field>> fields = {
        std::make_shared<Field>("id",           "INTEGER PRIMARY KEY AUTOINCREMENT"),
        std::make_shared<Field>("session_id",   "INTEGER", "sessions(id)"),
        std::make_shared<Field>("task_id",      "INTEGER", "tasks(id)")
    };
    std::vector<std::string> constraints = {};

    LinkSessionTask();
    LinkSessionTask(const std::vector<SqlValPair>&);
    ~LinkSessionTask();

    static std::vector<std::shared_ptr<LinkSessionTask>> filter(const std::string& = "");

    template<class T>
        static std::vector<std::shared_ptr<T>> get_all_linked_to(const std::string&, const std::string&);
    template<class T>
        static std::vector<std::shared_ptr<T>> get_all_linked_to(const std::string&, const long&);

    std::string                         get_name() const override;
    std::shared_ptr<Field>              get_field(const std::string&) const override;
    std::vector<std::shared_ptr<Field>> get_fields() const override;
    std::vector<std::string>            get_constraints() const override;
    void                                set_value(const std::string&, const std::string&) override;
    void                                set_value(const SqlValPair&) override;

    static const bool get_table_created() { return LinkSessionTask::table_created; }

    // TESTING
    #ifdef WTT_TEST
    FRIEND_TEST(LinkSessionTaskClassTest, constructor_test);
    FRIEND_TEST(LinkSessionTaskClassTest, constructor_test_missing_table);
    FRIEND_TEST(LinkSessionTaskClassTest, constructor_with_values_test);
    FRIEND_TEST(LinkSessionTaskClassTest, constructor_with_values_test_missing_table);
    #endif
};
