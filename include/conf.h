#pragma once

#ifdef DEBUG
    #ifdef WTT_TEST
        // #define DB ":memory:"
        #define DB "./unit_test.db"
        #define LOGFILE "./log/wtt_test.log"
        #define MOCKABLE virtual
    #else
        #define DB "./test.db"
        #define LOGFILE "./log/wtt.log"
        #define MOCKABLE
    #endif
    #define LOG_LEVEL spdlog::level::debug
    #define LOG_PATTERN "[%@] %+"
    #define NOTEFILE "./.wtt_note.tmp"
    #define EDITOR "/usr/bin/nvim"
#else
    #define LOG_LEVEL spdlog::level::warn
    #define LOG_PATTERN "%+"
    #define DB "/home/vi/.cache/wtt.db"
    #define LOGFILE "/home/vi/.cache/wtt.log"
    #define NOTEFILE "/home/vi/.cache/.wtt_note.tmp"
    #define EDITOR "/usr/bin/nvim"
    #define MOCKABLE
#endif
