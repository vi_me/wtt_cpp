#pragma once

#include <memory>
#include <string>
#include <ctime>


#include "CLI/App.hpp"
#include "CLI/Option.hpp"


#include "wtt_models/Session.hpp"


namespace wtt_app {
    struct IdFieldPair {
        std::string id_field_name;
        long        id_value;
    };

    class App {
    private:
        // Session related variables
        bool session__list_flag;
        bool session__list_linked_notes_flag;
        bool session__list_linked_note_ids_flag;

        // Note related variables
        long note__id = -1;
        std::string note__text;
        bool note__list_flag;
        bool note__cnotents_flag;
        bool note__edit_flag;

        // Task related variables
        long task__id = -1;
        bool task__list_flag;
        std::string task__status;
        bool task__list_linked_notes_flag;
        bool task__list_linked_note_ids_flag;
        bool task__description_flag;
        bool task__edit_flag;

        // General variables
        long log_max_size = 1048576 * 5;
        std::shared_ptr<CLI::App> cli_app = nullptr;

    public:
        App();
        ~App();
        int run(int argc, char **argv);

    private:
        void init_logger();
        void init_cli();
        void init_session_cli();
        void init_note_cli();
        void init_task_cli();
    };

    std::shared_ptr<Session> get_active_session();
    void start_session(const std::string& = "");
    void end_session(const std::string& = "");
    void pause_session(const std::string& = "");
    void unpause_session(const std::string& = "");
    void print_related_notes(const std::string&, const std::string&, const bool& = false);
    void print_related_notes(const std::string&, const long&, const bool& = false);


    template<class MT>
    std::shared_ptr<MT> get_related_object(const long& = 0);
    template<class MT, class LT>
    long single_link(const long&, const long&, const std::string&);
    template<class MT>
    void single_unlink(const long&, const std::string&);
    template<class MT, class FMT, class SLT>
    void mtm_link(const IdFieldPair&, const IdFieldPair&);
    template<class MT>
    void mtm_unlink(const IdFieldPair&, const IdFieldPair&);
} // namespace wtt_app
