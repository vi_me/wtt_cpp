#pragma once

#ifdef WTT_TEST
#include "gtest/gtest_prod.h"
#endif

#include <vector>
#include <utility>
#include <string>
#include <memory>

extern "C" {
    #include "sqlite3/sqlite3.h"
}

#include "wtt_core/Field.hpp"

class Model {

private:
    static int models_count;
    static sqlite3 *db;

protected:

    static void create_table(
        const std::string&,
        const std::vector<std::shared_ptr<Field>>&,
        const std::vector<std::string> & = {}
    );
    static std::vector<SqlValPair>              get(const std::string&, const std::string &);
    static std::vector<std::vector<SqlValPair>> filter(const std::string&, const std::string & = "");
    static bool                                 table_exists(const std::string&);
    static bool                                 exists(const std::string&, const::std::string&);

public:

    Model();
    Model(const Model&);
    virtual ~Model();

    static void init_db();
    static void close_db();

    // DB related NOTE: consider adding bulk operations
    virtual void save();
    virtual void update();
    virtual void del();

    //
    // virtual std::shared_ptr<Model>              init_new_as_model() const = 0;  NOTE: consider removing
    virtual std::string                         get_name() const = 0;
    virtual std::shared_ptr<Field>              get_field(const std::string&) const = 0;
    virtual std::vector<std::shared_ptr<Field>> get_fields() const = 0;
    virtual std::vector<std::string>            get_constraints() const = 0;
    virtual void                                set_value(const std::string&, const std::string&) = 0;
    virtual void                                set_value(const SqlValPair&) = 0;

    // TESTING
    #ifdef WTT_TEST
protected:
    static void                                 drop_table(const std::string&);
public:
    FRIEND_TEST(SessionClassTest, constructor_test);
    FRIEND_TEST(SessionClassTest, constructor_test_missing_table);
    FRIEND_TEST(SessionClassTest, constructor_with_values_test);
    FRIEND_TEST(SessionClassTest, constructor_with_values_test_missing_table);
    FRIEND_TEST(NoteClassTest, constructor_test);
    FRIEND_TEST(NoteClassTest, constructor_test_missing_table);
    FRIEND_TEST(NoteClassTest, constructor_with_values_test);
    FRIEND_TEST(NoteClassTest, constructor_with_values_test_missing_table);
    FRIEND_TEST(TaskClassTest, constructor_test);
    FRIEND_TEST(TaskClassTest, constructor_test_missing_table);
    FRIEND_TEST(TaskClassTest, constructor_with_values_test);
    FRIEND_TEST(TaskClassTest, constructor_with_values_test_missing_table);
    FRIEND_TEST(LinkSessionTaskClassTest, constructor_test);
    FRIEND_TEST(LinkSessionTaskClassTest, constructor_test_missing_table);
    FRIEND_TEST(LinkSessionTaskClassTest, constructor_with_values_test);
    FRIEND_TEST(LinkSessionTaskClassTest, constructor_with_values_test_missing_table);
    #endif
};
