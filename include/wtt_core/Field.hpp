#pragma once

#include <string>


struct SqlValPair {
    std::string name, value;
};


class Field {

public:

    std::string value = "";
    std::string name = "";
    std::string sql_type = "";
    std::string fk = "";

    Field() = delete;
    Field(std::string, std::string);
    Field(std::string, std::string, std::string);
    Field(const Field&);
    ~Field();

    void set_value(const std::string&);
};
