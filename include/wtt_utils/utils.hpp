#pragma once
#include <string>

#include "CLI/CLI.hpp"
#include "spdlog/spdlog.h"

namespace wtt_utils
{
    void init_note_cli(CLI::App &);
    void init_task_cli(CLI::App &);
    std::string get_current_timestamp_string();
    int64_t get_current_timestamp();
    std::ostringstream text_from_editor();
    std::string stringsafe(const std::string &);
    std::string strdecode(const std::string &);
} // namespace wtt_utils
