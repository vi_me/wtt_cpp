workspace "wtt"
   startproject "wtt"
   configurations { "Debug", "Release", "Coverage"}

   cleancommands {
      "{RMDIR} " .. path.join("include", "sqlite3"),
      "{RMDIR} " .. "*.gcov",
      "{RMDIR} " .. path.join("coverage", "*.html"),
      "{RMDIR} " .. path.join("coverage", "*.css"),
      "{RMDIR} " .. "unit_test.db",
      "{RMDIR} " .. path.join("obj", "Debug"),
      "{RMDIR} " .. path.join("obj", "Release"),
      "{RMDIR} " .. path.join("obj", "Coverage"),
      "{RMDIR} " .. path.join("obj", "sqlite3"),
      "{RMDIR} " .. path.join("lib", "*.a"),
   }

project "wtt"
   kind "ConsoleApp"
   language "C++"
   cppdialect "C++17"
   architecture "x86_64"

   dependson {
      "sqlite",
      "spdlog",
      "CLI",
   }
   libdirs { "lib" }
   includedirs { "include" }

   targetdir ( path.join("bin", "%{cfg.buildcfg}") )
   objdir ( path.join("obj", "%{cfg.buildcfg}") )

   files { path.join("src", "**.cpp") }

   filter "configurations:Debug"
      defines { "DEBUG", "SPDLOG_ACTIVE_LEVEL=SPDLOG_LEVEL_DEBUG"}
      symbols "On"

   filter "configurations:Release"
      defines { "NDEBUG", "SPDLOG_ACTIVE_LEVEL=SPDLOG_LEVEL_DEBUG"}

   filter "system:linux"
      links { "sqlite", "spdlog", "stdc++fs", "dl", "pthread" }
      buildoptions {"-std=c++2a", "-save-temps" }

   filter "system:macosx"
      architecture "arm64"
      links { "sqlite", "spdlog", "dl", "pthread" }
      buildoptions "-std=c++2a"
      optimize "Off"

   filter "system:windows"
      defines { "WIN" }
      cppdialect "C++17"
      links { "sqlite", "spdlog" }


project "wtt_test"
   kind "ConsoleApp"
   language "C++"
   cppdialect "C++17"
   architecture "x86_64"

   dependson {
      "sqlite",
      "spdlog",
      "CLI",
      "googletest"
   }
   libdirs { "lib" }
   includedirs { "include" }

   targetdir ( path.join("bin", "%{cfg.buildcfg}") )
   objdir ( path.join("obj", "%{cfg.buildcfg}") )

   files {
      path.join("test", "**.cpp"),
      path.join("src/core", "**.cpp"),
      path.join("src/models", "**.cpp"),
      path.join("src/wtt_app", "**.cpp"),
      path.join("src/utils", "**.cpp")
   }

   

   filter "configurations:Debug"
      defines { "DEBUG", "WTT_TEST", "SPDLOG_ACTIVE_LEVEL=SPDLOG_LEVEL_DEBUG" }
      symbols "On"
      postbuildcommands (
         "%{cfg.buildtarget.abspath}",
         "{RMDIR} " .. "unit_test.db"
      )

   filter "configurations:Release"
      defines { "NDEBUG", "WTT_TEST", "SPDLOG_ACTIVE_LEVEL=SPDLOG_LEVEL_ERROR" }
      optimize "On"

   filter "configurations:Coverage"
      defines { "DEBUG", "WTT_TEST", "SPDLOG_ACTIVE_LEVEL=SPDLOG_LEVEL_DEBUG" }
      symbols "On"
      postbuildcommands {
         "%{cfg.buildtarget.abspath}",
         "{RMDIR} " .. "unit_test.db",
         "gcovr -e dependencies -e include -e test --exclude-throw-branches --exclude-unreachable-branches --decisions --html-nested " .. path.join("coverage", "coverage.html") .. " %{wks.location}"
         -- "gcovr -e dependencies -e include -e test --exclude-throw-branches --exclude-unreachable-branches --decisions --html-theme github.dark-green --html-nested " .. path.join("coverage", "coverage.html") .. " %{wks.location}"
      }

   filter "system:linux"
      links { "sqlite", "spdlog", "gtest", "gmock", "stdc++fs", "dl", "pthread", "gcov" }
      buildoptions { "-std=c++2a", "-fprofile-arcs", "-ftest-coverage", "-O0", "-save-temps" }

   filter "system:windows"
      defines { "WIN" }
      cppdialect "C++17"
      links { "sqlite", "spdlog", "gtest", "gmock" }

   filter "system:macosx"
   links { "sqlite", "spdlog", "gtest", "gmock", "dl", "pthread" }
      architecture "arm64"


project "sqlite"
   kind "StaticLib"
   language "C"
   architecture "x86_64"

   includedirs { path.join("dependencies", "sqlite3", "include") }

   targetdir "lib"
   objdir ( path.join("obj", "sqlite3") )

   files {
      path.join("dependencies", "sqlite3", "**.c")
   }

   postbuildcommands {
      "{COPY} " .. path.join("dependencies", "sqlite3", "include") .. " " .. path.join("include", "sqlite3")
   }

   filter "configurations:Debug"
      defines { "DEBUG" }
      symbols "On"

   filter "configurations:Release"
      defines { "NDEBUG" }
      optimize "On"

   filter "system:linux"
      links { "dl", "pthread" }

   filter "system:macosx"
      architecture "arm64"
      links { "dl", "pthread" }


project "spdlog"
   kind "Makefile"
   targetdir "lib"

   local p = path.getabsolute(".")
   local dep_dir = path.join("dependencies", "spdlog")
   local build_dir = path.join("dependencies", "spdlog", "_builds")
   local inc_dir = path.join("include", "spdlog")
   local rm_cmake = path.join("%{cfg.targetdir}", "cmake")
   local rm_pkgconfig = path.join("%{cfg.targetdir}", "pkgconfig")
   local rm_lib = path.join("%{cfg.targetdir}", "libspdlogd.a")

   buildcommands {
      "{CHDIR} " .. path.join(p, dep_dir) .. " && cmake -H. -B_builds –DCMAKE_CXX_STANDARD=17 –DCMAKE_CXX_STANDARD_REQUIRED=17 -DCMAKE_INSTALL_PREFIX=" .. p .. " -DCMAKE_BUILD_TYPE=%{cfg.buildcfg}",
      "{CHDIR} " .. path.join(p, dep_dir) .. " && cmake --build _builds --target install"
   }

   rebuildcommands {
      "{RMDIR} " .. build_dir,
      "{RMDIR} " .. inc_dir,
      "{RMDIR} " .. rm_cmake,
      "{RMDIR} " .. rm_pkgconfig,
      "{DELETE} " .. rm_lib,
      "{CHDIR} " .. path.join(p, dep_dir) .. " && cmake -H. -B_builds –DCMAKE_CXX_STANDARD=17 –DCMAKE_CXX_STANDARD_REQUIRED=17 -DCMAKE_INSTALL_PREFIX=".. p .. " -DCMAKE_BUILD_TYPE=%{cfg.buildcfg}",
      "{CHDIR} " .. path.join(p, dep_dir) .. " && cmake --build _builds --target install"
   }

   cleancommands {
      "{RMDIR} " .. build_dir,
      "{RMDIR} " .. inc_dir,
      "{RMDIR} " .. rm_cmake,
      "{RMDIR} " .. rm_pkgconfig,
      "{DELETE} " .. rm_lib
   }


project "googletest"
   targetdir "lib"
   kind "Makefile"

   local dep_dir = path.join("dependencies", "googletest")

   buildcommands {
      "{CHDIR} " .. path.join(p, dep_dir) .. " && cmake -S " .. path.join(p, dep_dir) .. " -B " .. path.join(p, dep_dir) .. " && make",
      "{COPY} " .. path.join("dependencies", "googletest", "googletest", "include", "*") .. " " .. "include",
      "{COPY} " .. path.join("dependencies", "googletest", "googlemock", "include", "*") .. " " .. "include",
      "{COPY} " .. path.join("dependencies", "googletest", "lib", "*") .. " " .. "lib"
   }

   rebuildcommands {
      "{CHDIR} " .. path.join(p, dep_dir) .. " && cmake . && make",
      "{COPY} " .. path.join("dependencies", "googletest", "googletest", "include", "*") .. " " .. "include",
      "{COPY} " .. path.join("dependencies", "googletest", "googlemock", "include", "*") .. " " .. "include",
      "{COPY} " .. path.join("dependencies", "googletest", "lib", "*") .. " " .. "lib"
   }

   filter "system:macosx"
      architecture "arm64"

   cleancommands {
   }


project "CLI"
   kind "Makefile"

   local dep_dir = path.join("dependencies", "CLI11", "include", "CLI", "*")
   local inc_dir = path.join("include", "CLI")

   buildcommands {
      "{MKDIR} " .. inc_dir,
      "{COPY} " .. dep_dir .. " " .. inc_dir
   }

   rebuildcommands {
      "{RMDIR} " .. inc_dir,
      "{MKDIR} " .. inc_dir,
      "{COPY} " .. dep_dir .. " " .. inc_dir
   }

   cleancommands {
      "{RMDIR} " .. inc_dir
   }
