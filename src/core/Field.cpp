#include <string>

#include "spdlog/spdlog.h"

#include "wtt_core/Field.hpp"
#include "conf.h"


Field::Field(std::string name_, std::string sql_type_):
    name(name_), sql_type(sql_type_) {
    SPDLOG_DEBUG(
        "Field \"{}\" Created with {}, {}.",
        this->name,
        this->sql_type,
        this->fk
    );
}


Field::Field(std::string name_, std::string sql_type_, std::string fk_):
    name(name_), sql_type(sql_type_), fk(fk_) {
    SPDLOG_DEBUG(
        "Field \"{}\" Created with {}, {}.",
        this->name,
        this->sql_type,
        this->fk
    );
}


Field::Field(const Field& obj):
    name(obj.name), sql_type(obj.sql_type), fk(obj.fk) {
    SPDLOG_DEBUG(
        "Field \"{}\" Copied with {}, {}.",
        this->name,
        this->sql_type,
        this->fk
    );
}


Field::~Field() {
    SPDLOG_DEBUG("Field {} Destroyed.", this->name);
}


void Field::set_value(const std::string &val) {
    this->value = val;
    SPDLOG_DEBUG(
        "Value of {} is set to {}.",
        this->name,
        this->value
    );
}
