#include <cstring>
#include <string>
#include <iostream>
#include <sstream>
#include <memory>
#include <filesystem>
#include <stdexcept>

#include "spdlog/spdlog.h"

extern "C" {
    #include "sqlite3/sqlite3.h"
}

#include "wtt_core/Field.hpp"
#include "wtt_core/Model.hpp"
#include "conf.h"


int Model::models_count = 0;
sqlite3* Model::db = nullptr;

Model::Model() {
    if (Model::models_count == 0 || !Model::db) {
        Model::init_db();
    }
    Model::models_count++;
    SPDLOG_DEBUG("Model Created");
}


Model::Model(const Model &other) {
    if (Model::models_count == 0 || !Model::db) {
        Model::init_db();
    }
    Model::models_count++;
    SPDLOG_DEBUG("Model Copied");
}


Model::~Model() {
    Model::models_count--;
    if (Model::models_count == 0) {
        Model::close_db();
    }
    SPDLOG_DEBUG("Model Destyroyed");
}


void Model::init_db() {
    int rc = 0;
    if (!Model::db) {
        if (strcmp(DB, ":memory:")) {
            rc = sqlite3_open(DB, &Model::db);
        } else {
            rc = sqlite3_open(std::filesystem::absolute(DB).string().c_str(), &Model::db);
        }

        if (rc && sqlite3_errcode(Model::db)) {
            std::string err_msg = sqlite3_errmsg(Model::db);
            SPDLOG_ERROR("Error opening DB : {}",  err_msg);
            throw std::runtime_error("ERROR opening DB: " + err_msg);
        } else {
            SPDLOG_DEBUG("Database opened successfully");
        }
    }
}


void Model::close_db() {
    if (Model::models_count == 0) {
        if (sqlite3_close(Model::db) != SQLITE_OK) {
            SPDLOG_ERROR("Error closing the database: {}", sqlite3_errmsg(Model::db));
        } else {
            Model::db = nullptr;
            SPDLOG_DEBUG("Database closed");
        }
    } else {
        SPDLOG_ERROR("Closing DB with existing Models");
    }
}


void Model::create_table(
    const std::string &name,
    const std::vector<std::shared_ptr<Field>> &fields,
    const std::vector<std::string> &constraints
) {

    std::string sql;
    std::string relations = "";
    std::string constraints_str = "";
    char *ErrMsg = 0;
    int rc;

    // Handle DB connection in static method
    Model::init_db();

    sql = "CREATE TABLE IF NOT EXISTS " + name + " (";

    std::string divider = "";
    for (std::shared_ptr<Field> x : fields) {
        sql += divider + x->name + " " + x->sql_type;
        divider = ", ";

        if (x->fk.length() != 0) {
            relations += ", FOREIGN KEY(" + x->name + ") REFERENCES " + x->fk;
        }
    }
    for (const std::string &constraint : constraints) {
        constraints_str += ", " + constraint;
    }

    sql += relations + constraints_str + ");";

    SPDLOG_DEBUG("[Executing] : {}", sql);

    rc = sqlite3_exec(Model::db, sql.c_str(), NULL, 0, &ErrMsg);

    if (rc != SQLITE_OK) {
        std::string err = ErrMsg;
        sqlite3_free(ErrMsg);
        SPDLOG_ERROR("SQL error: {}",  err);
        throw std::runtime_error("Error while creating table: " + err);
    } else {
        SPDLOG_INFO("Table {} created successfully.", name);
    }
}


std::vector<SqlValPair> Model::get(const std::string &name, const std::string &id) {

    std::string sql;
    char *ErrMsg = 0;
    int rc;

    // Handle DB connection in static method
    Model::init_db();

    // String formatting in formatting std library or boost formatting
    std::ostringstream result;
    result << "SELECT * from " << name << " WHERE id=" << id << ";";
    sql = result.str();

    std::vector<SqlValPair> *out = new std::vector<SqlValPair>();

    SPDLOG_DEBUG("[Executing] : {}", sql);

    rc = sqlite3_exec(
        Model::db,
        sql.c_str(),
        [](void *data, int argc, char **argv, char **azColName) {
            SPDLOG_DEBUG("Callback.");
            std::vector<SqlValPair> *out = reinterpret_cast<std::vector<SqlValPair> *>(data);
            for(int i = 0; i < argc; i++) {
                out->push_back({
                    azColName[i],
                    (argv[i] ? argv[i] : "NULL")
                });
            }
            return 0;
        },
        (void*)out,
        &ErrMsg
    );

    if (rc != SQLITE_OK) {
        std::string err = ErrMsg;
        sqlite3_free(ErrMsg);
        SPDLOG_ERROR("SQL error: {}",  err);
        throw std::runtime_error("Error while getting Model: " + err);
    } else {
        SPDLOG_INFO("Get operation for {} successful", name);
    }

    if (out->size() < 1) {
        std::ostringstream error;
        error << "No object with id=" << id << " in table " << name << ".";
        SPDLOG_ERROR(error.str());
        throw std::runtime_error(error.str());
    }

    return *out;
}


std::vector<std::vector<SqlValPair>> Model::filter(const std::string &name, const std::string &filter) {

    std::string sql;
    char *ErrMsg = 0;
    int rc;

    // Handle DB connection in static method
    Model::init_db();

    // String formatting in formatting std library or boost formatting
    std::ostringstream result;
    result << "SELECT * FROM " << name << ((filter.empty()) ? "" : " WHERE " + filter) << ";";
    sql = result.str();

    std::vector<std::vector<SqlValPair>> *out = new std::vector<std::vector<SqlValPair>>();

    SPDLOG_DEBUG("[Executing] : {}", sql);

    rc = sqlite3_exec(
        Model::db,
        sql.c_str(),
        [](void *data, int argc, char **argv, char **azColName) {
            SPDLOG_DEBUG("Callback.");
            std::vector<std::vector<SqlValPair>> *out = reinterpret_cast<std::vector<std::vector<SqlValPair>> *>(data);
            std::vector<SqlValPair> temp;
            for(int i = 0; i < argc; i++) {
                temp.push_back({
                    azColName[i],
                    (argv[i] ? argv[i] : "NULL")
                });
            }
            out->push_back(temp);
            return 0;
        },
        (void*)out,
        &ErrMsg
    );

    if (rc != SQLITE_OK) {
        std::string err = ErrMsg;
        sqlite3_free(ErrMsg);
        SPDLOG_ERROR("SQL error: {}",  err);
        throw std::runtime_error("Error while filtering Model: " + err);
    } else {
        SPDLOG_INFO("Filter operation for {} successful", name);
    }

    return *out;
}


bool Model::table_exists(const std::string& name) {

    std::string sql;
    char *ErrMsg = 0;
    int rc;

    // Handle DB connection in static method
    Model::init_db();

    // String formatting in formatting std library or boost formatting
    std::ostringstream result;
    result << "SELECT EXISTS(SELECT 1 FROM sqlite_master WHERE type='table' AND name='" << name << "');";
    sql = result.str();

    int out = 0;

    SPDLOG_DEBUG("[Executing] : {}", sql);

    rc = sqlite3_exec(
        Model::db,
        sql.c_str(),
        [](void *data, int argc, char **argv, char **azColName) {
            SPDLOG_DEBUG("Callback.");
            int *out = (int *)data;
            *out = std::atoi(argv[0]);
            return 0;
        },
        &out,
        &ErrMsg
    );

    if (rc != SQLITE_OK) {
        std::string err = ErrMsg;
        sqlite3_free(ErrMsg);
        SPDLOG_ERROR("SQL error: {}",  err);
        throw std::runtime_error("Error while checking if table exists: " + err);
    } else {
        SPDLOG_INFO("Table_exists operation for {} successful", name);
    }

    return out > 0;
}


bool Model::exists(const std::string& name, const std::string& id) {
    if (Model::table_exists(name)) {
        std::string sql;
        char *ErrMsg = 0;
        int rc;

        // Handle DB connection in static method
        Model::init_db();

        // String formatting in formatting std library or boost formatting
        std::ostringstream result;
        result << "SELECT EXISTS(SELECT 1 FROM " << name << " WHERE id=" << id << ");";
        sql = result.str();

        int out = 0;

        SPDLOG_DEBUG("[Executing] : {}", sql);

        rc = sqlite3_exec(
            Model::db,
            sql.c_str(),
            [](void *data, int argc, char **argv, char **azColName) {
                SPDLOG_DEBUG("Callback.");
                int *out = (int *)data;
                *out = std::atoi(argv[0]);
                return 0;
            },
            &out,
            &ErrMsg
        );

        if (rc != SQLITE_OK) {
            std::string err = ErrMsg;
            sqlite3_free(ErrMsg);
            SPDLOG_ERROR("SQL error: {}",  err);
            throw std::runtime_error("Error while checking if Model exists: " + err);
        } else {
            SPDLOG_INFO("Exists operation for {} successful", name);
        }

        return out > 0;

    } else {
        return false;
    }
}


void Model::save() {
    if (this->get_field("id")->value != "") {
        SPDLOG_INFO(
            "Object of type {} with ID {} already exists, UPDATING...",
            this->get_name(), this->get_field("id")->value
        );
        this->update();
        return;
    }

    std::string sql;
    char *ErrMsg = 0;
    std::string columns = "(";
    std::string values = "(";
    std::string sep = "";
    int rc;

    for (auto field : this->get_fields()) {
        if (field->value != "") {
            columns += sep + field->name;
            values += sep + "\"" + field->value + "\"";
            sep = ", ";
        }
    }

    columns += ")";
    values += ")";

    sql = "INSERT INTO " + this->get_name() + " " + ((columns != "()") ? columns : "DEFAULT") + " VALUES " + ((values != "()") ? values : "") + ";"
        + "SELECT * FROM " + this->get_name() + " WHERE id=last_insert_rowid();";


    SPDLOG_DEBUG("[Executing] : {}", sql);

    std::vector<SqlValPair> *out = new std::vector<SqlValPair>();

    rc = sqlite3_exec(Model::db, sql.c_str(),
    [](void *data, int argc, char **argv, char **azColName) {
        SPDLOG_DEBUG("Callback.");
        std::vector<SqlValPair> *out = reinterpret_cast<std::vector<SqlValPair> *>(data);
        for(int i = 0; i < argc; i++) {
            out->push_back({
                azColName[i],
                (argv[i] ? argv[i] : "NULL")
            });
        }
        return 0;
    }, (void*)out, &ErrMsg);

    if (rc != SQLITE_OK) {
        std::string err = ErrMsg;
        sqlite3_free(ErrMsg);
        SPDLOG_ERROR("SQL error: {}",  err);
        throw std::runtime_error("Error while saving Model: " + err);
    } else {
        SPDLOG_INFO("Save operation for {} successful", this->get_name());
    }

    for (auto val_pair : *out) {
        this->set_value(val_pair);
    }
}


void Model::update() {
    std::string sql;
    char *ErrMsg = 0;
    int rc;

    sql = "UPDATE " + this->get_name() + " SET ";

    std::string id;

    std::string divider = "";
    for (const std::shared_ptr<Field> x : this->get_fields()) {
        if (x->name == "id") {
            id = x->value;
        } else {
            sql += divider + x->name + " = \"" + x->value + "\"";
            divider = ", ";
        }
    }

    sql += " WHERE id = " + id + ";";

    SPDLOG_DEBUG("[Executing] : {}", sql);

    rc = sqlite3_exec(Model::db, sql.c_str(), NULL, 0, &ErrMsg);

    if (rc != SQLITE_OK) {
        std::string err_msg = ErrMsg;
        sqlite3_free(ErrMsg);
        SPDLOG_ERROR("SQL error: {}",  err_msg);
        throw std::runtime_error("Error while updating Model: " + err_msg);
    } else {
        SPDLOG_INFO("Update operation for {} successful", this->get_name());
    }
}


void Model::del() {
    std::string sql;
    char *ErrMsg = 0;
    int rc;

    sql = "DELETE FROM " + this->get_name() + " WHERE id=" + this->get_field("id")->value + ";";

    SPDLOG_DEBUG("[Executing] : {}", sql);

    rc = sqlite3_exec(Model::db, sql.c_str(), NULL, 0, &ErrMsg);

    if (rc != SQLITE_OK) {
        std::string err_msg = ErrMsg;
        sqlite3_free(ErrMsg);
        SPDLOG_ERROR("SQL error: {}",  err_msg);
        throw std::runtime_error("Error while deleting Model: " + err_msg);
    } else {
        SPDLOG_INFO("Delete operation for {} successful", this->get_name());
    }
}


#ifdef WTT_TEST  // TODO: find better solution for mocking in tests
void Model::drop_table(const std::string &name) {
    std::string sql;
    char *ErrMsg = 0;
    int rc;

    // Handle DB connection in static method
    Model::init_db();

    sql = "DROP TABLE IF EXISTS " + name + ";";

    SPDLOG_DEBUG("[Executing] : {}", sql);

    rc = sqlite3_exec(Model::db, sql.c_str(), NULL, 0, &ErrMsg);

    if (rc != SQLITE_OK) {
       std::string err_msg = ErrMsg;
        sqlite3_free(ErrMsg);
        SPDLOG_ERROR("SQL error: {}",  err_msg);
        throw std::runtime_error("Error while dropping table: " + err_msg);
    } else {
        SPDLOG_DEBUG("Table {} dropped successfully.", name);
    }
}
#endif
