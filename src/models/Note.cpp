#include <filesystem>
#include <vector>
#include <memory>

#include "spdlog/spdlog.h"

#include "wtt_models/Note.hpp"
#include "wtt_models/Session.hpp"
#include "wtt_models/Task.hpp"
#include "wtt_core/Model.hpp"
#include "conf.h"


bool Note::table_created = false;


bool Note::check_table() {
    return table_created ? table_created : Model::table_exists(Note::name);
}


Note::Note() {
    table_created =Note::check_table(); 
    if (!table_created) {
        SPDLOG_DEBUG("Creating notes table from constructor");
        Model::create_table(this->name, this->fields);
        table_created = Model::table_exists(this->name);
    }
    SPDLOG_INFO("Note object reated");
}


Note::Note(const std::vector<SqlValPair> &f_vals) {
    table_created = Note::check_table();
    if (!table_created) {
        SPDLOG_DEBUG("Creating notes table from constructor");
        Model::create_table(this->name, this->fields);
        table_created = Model::table_exists(this->name);
    }

    SPDLOG_DEBUG("Note object created");

    SPDLOG_DEBUG("Setting {} \"Note\" values", f_vals.size());
    for (const SqlValPair & i : f_vals) {
        SPDLOG_DEBUG("Setting {} to {}", i.name, i.value);
        this->set_value(i);
    }
}


Note::~Note() {
    SPDLOG_DEBUG("Note object destroyed");
}


std::shared_ptr<Note> Note::get(const long &id) {
    return Note::get(std::to_string(id));
}


std::shared_ptr<Note> Note::get(const std::string &id) {

    SPDLOG_DEBUG("Getting Note with id {}", id);

    std::shared_ptr<Note> n = std::make_shared<Note>();
    std::vector<SqlValPair> values = Model::get(n->name, id);  // Thows exception if not found

    for (const SqlValPair &i : values) {
        SPDLOG_DEBUG("Setting {} to {} for Note object", i.name, i.value);
        n->set_value(i);
    }
    return n;
}

std::vector<std::shared_ptr<Note>> Note::filter(const std::string &filter) {
    std::vector<std::shared_ptr<Note>> out;

    SPDLOG_DEBUG("Filtering Notes with \"{}\"", filter);

    if (!Note::check_table()) {
        return out;
    }

    std::vector<std::vector<SqlValPair>> filtered = Model::filter("notes", filter);
    SPDLOG_DEBUG("Found {} Notes", filter.size());

    out.reserve(filtered.size());

    for (const std::vector<SqlValPair> &i : filtered) {
        out.emplace_back(std::make_shared<Note>(i));
    }

    return out;
}


void Note::set_value(const std::string &f_name, const std::string &val) {

    SPDLOG_DEBUG("Setting field {} in Note", f_name);

    std::vector<std::shared_ptr<Field>>::iterator it = std::find_if(
        this->fields.begin(),
        this->fields.end(),
        [&](std::shared_ptr<Field> obj) {
            return obj->name == f_name;
        }
    );

    if (it != this->fields.end()) {
        auto index = std::distance(this->fields.begin(), it);
        this->fields[index]->set_value(val);
        SPDLOG_DEBUG("Note field {} set to {} ", f_name, val);
    } else {
        SPDLOG_ERROR("Field \"{}\" not found in Note model", f_name);
        throw std::runtime_error("Field \"" + f_name + "\" not found in Note model");
    }
}


void Note::set_value(const SqlValPair &val) {
    this->set_value(val.name, val.value);
}


std::string Note::get_name() const {
    SPDLOG_DEBUG("Getting table name for Note");
    return this->name;
}


std::shared_ptr<Field> Note::get_field(const std::string &f_name) const {
    SPDLOG_DEBUG("Getting field {} in Note", f_name);

    auto it = std::find_if(
        this->fields.begin(),
        this->fields.end(),
        [&](std::shared_ptr<Field> obj) {
            return obj->name == f_name;
        }
    );

    if (it != this->fields.end()) {
        SPDLOG_DEBUG("Field {} found in Note", f_name);
        auto index = std::distance(this->fields.begin(), it);
        return this->fields[index];
    } else {
        SPDLOG_ERROR("Field \"{}\" not found in Note model", f_name);
        throw std::runtime_error("Field \"" + f_name + "\" not found in Note model");

    }
}


std::vector<std::shared_ptr<Field>> Note::get_fields() const {
    SPDLOG_DEBUG("Getting fields for Note model");
    return this->fields;
}


std::vector<std::string> Note::get_constraints() const {
    SPDLOG_DEBUG("Getting constraints for Notes model");
    return this->constraints;
}


std::ostream& operator<<(std::ostream& os, const Note& note) {
    os << "ID: " << note.get_field("id")->value;

    if (note.get_field("session_id")->value != "NULL" && note.get_field("session_id")->value != "") {
        os << "; Linked Session ID: " << Session::get(note.get_field("session_id")->value)->get_field("id")->value;
    }

    if (note.get_field("task_id")->value != "NULL" && note.get_field("task_id")->value != "") {
        os << "; Linked Task ID: " << Task::get(note.get_field("task_id")->value)->get_field("id")->value;
    }

    return os;
}
