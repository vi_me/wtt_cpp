#include <filesystem>
#include <vector>
#include <memory>

#include "spdlog/spdlog.h"

#include "wtt_models/LinkSessionTask.hpp"
#include "wtt_models/Session.hpp"
#include "wtt_models/Task.hpp"
#include "wtt_core/Model.hpp"
#include "wtt_core/Field.hpp"
#include "conf.h"
#include "wtt_utils/utils.hpp"


bool Task::table_created = false;


bool Task::check_table() {
    return table_created ? table_created : Model::table_exists(Task::name);
}


Task::Task() {
    table_created = Task::check_table();
    if (!table_created) {
        SPDLOG_DEBUG("Calling Task::create_table...");
        Model::create_table(this->name, this->fields);
        table_created = Model::table_exists(this->name);
    }
    SPDLOG_DEBUG("Task Created.");
}


Task::Task(const std::vector<SqlValPair> &f_vals) {
    table_created = Task::check_table();
    if (!table_created) {
        SPDLOG_DEBUG("Calling Task::create_table...");
        Model::create_table(this->name, this->fields);
        table_created = Model::table_exists(this->name);
    }

    SPDLOG_DEBUG("Session Created.");

    SPDLOG_DEBUG("Setting {} \"Task\" values...", f_vals.size());
    for (const SqlValPair & i : f_vals) {
        SPDLOG_DEBUG("Setting {} to {}", i.name, i.value);
        this->set_value(i);
    }
}


Task::~Task() {
    SPDLOG_DEBUG("Task Destroyed.");
}


std::shared_ptr<Task> Task::get(const long &id) {
    return Task::get(std::to_string(id));
}


std::shared_ptr<Task> Task::get(const std::string &id) {

    SPDLOG_DEBUG("Getting Task with id={}", id);

    std::shared_ptr<Task> t = std::make_shared<Task>();
    std::vector<SqlValPair> values = Model::get(t->name, id);  // Thows exception if not found

    for (const SqlValPair &i : values) {
        SPDLOG_DEBUG("Setting {} to {}", i.name, i.value);
        t->set_value(i);
    }
    return t;
}


std::vector<std::shared_ptr<Task>> Task::filter(const std::string &filter) {
    std::vector<std::shared_ptr<Task>> out;

    SPDLOG_DEBUG("Filtering Tasks with \"{}\"", filter);

    if (!Task::check_table()) {
        return out;
    }
    std::vector<std::vector<SqlValPair>> filtered = Model::filter("tasks", filter);
    SPDLOG_DEBUG("Found {} Tasks", filter.size());

    out.reserve(filtered.size());
    for (const std::vector<SqlValPair> &i : filtered) {
        out.emplace_back(std::make_shared<Task>(i));
    }
    return out;
}

bool Task::exists(const long& id) {
    return Model::exists(Task::name, std::to_string(id));
}


bool Task::exists(const std::string& id) {
    return Model::exists(Task::name, id);
}


// void Task::save() {
// }


// void Task::update() {
// }


// void Task::del() {
// }


void Task::set_value(const std::string &f_name, const std::string &val) {

    SPDLOG_DEBUG("Trying to set field {} in Task", f_name);

    std::vector<std::shared_ptr<Field>>::iterator it = std::find_if(
        this->fields.begin(),
        this->fields.end(),
        [&](std::shared_ptr<Field> obj) {
            return obj->name == f_name;
        }
    );
    if (it != this->fields.end()) {
        SPDLOG_DEBUG("Field {} found in Task", f_name);
        auto index = std::distance(this->fields.begin(), it);
        this->fields[index]->set_value(val);
        SPDLOG_DEBUG("Task value {} set to {}.", f_name, val);
    } else {
        SPDLOG_ERROR("Field \"{}\" not found in Task model", f_name);
        throw std::runtime_error("Field \"" + f_name + "\" not found in Task model");
    }
}


void Task::set_value(const SqlValPair &val) {
    this->set_value(val.name, val.value);
}


std::string Task::get_name() const {
    return this->name;
}


std::shared_ptr<Field> Task::get_field(const std::string &f_name) const {
    SPDLOG_DEBUG("Trying to get field {} in Session", f_name);

    auto it = std::find_if(
        this->fields.begin(),
        this->fields.end(),
        [&](std::shared_ptr<Field> obj) {
            return obj->name == f_name;
        }
    );
    if (it != this->fields.end()) {
        SPDLOG_DEBUG("Field {} found in Task", f_name);
        auto index = std::distance(this->fields.begin(), it);
        return this->fields[index];
    } else {
        SPDLOG_ERROR("Field not found in Task");
        throw std::runtime_error("Field not found");

    }
}


std::vector<std::shared_ptr<Field>> Task::get_fields() const {
    return this->fields;
}


std::vector<std::string> Task::get_constraints() const {
    return this->constraints;
}


std::ostream& operator<<(std::ostream& os, const Task& task) {
    os << "ID: " << task.get_field("id")->value;
    std::string divider = "";
    std::vector<std::shared_ptr<Session>> linked_sessions = LinkSessionTask::get_all_linked_to<Session>("task", task.get_field("id")->value);
    if (linked_sessions.size() > 0) {
        os << "; Linked Sessions : ";
        for (auto session : linked_sessions) {
            os << divider << session->get_field("id")->value;
            divider = ", ";
        }
    }
    os << std::endl <<  "Status: " << task.get_field("status")->value;
    os << std::endl <<  "Summary: " << wtt_utils::strdecode(task.get_field("summary")->value);
    return os;
}

