#include <filesystem>
#include <string>
#include <vector>
#include <memory>
#include <stdexcept>

#include "spdlog/spdlog.h"
#include "wtt_utils/utils.hpp"

extern "C" {
    #include "sqlite3/sqlite3.h"
}

#include "wtt_models/Session.hpp"
#include "wtt_core/Model.hpp"
#include "wtt_core/Field.hpp"
#include "conf.h"


bool Session::table_created = false;


bool Session::check_table() {
    return table_created ? table_created : Model::table_exists(Session::name);
}


Session::Session() {
    table_created = Session::check_table();
    if (!table_created) {
        SPDLOG_DEBUG("Creating sessions table from constructor");
        Model::create_table(this->name, this->fields, this->constraints);
        table_created = Model::table_exists(this->name);
    }
    SPDLOG_DEBUG("Session object created");
}

Session::Session(const std::vector<SqlValPair> &f_vals) {
    table_created = Session::check_table();
    if (!table_created) {
        SPDLOG_DEBUG("Creating sessions table from constructor");
        Model::create_table(this->name, this->fields, this->constraints);
        table_created = Model::table_exists(this->name);
    }

    SPDLOG_DEBUG("Session object created");

    SPDLOG_DEBUG("Setting {} \"Session\" values", f_vals.size());
    for (const SqlValPair &i : f_vals) {
        SPDLOG_DEBUG("Setting {} to {} for Session object", i.name, i.value);
        this->set_value(i);
    }
}


Session::~Session() {
    SPDLOG_DEBUG("Session object destyroyed");
}


std::shared_ptr<Session> Session::get(const long &id) {
    return Session::get(std::to_string(id));
}


std::shared_ptr<Session> Session::get(const std::string &id) {

    SPDLOG_DEBUG("Getting Session with id {}", id);

    std::shared_ptr<Session> s = std::make_shared<Session>();
    std::vector<SqlValPair> values = Model::get(s->name, id);  // Thows exception if not found

    for (const SqlValPair &i : values) {
        SPDLOG_DEBUG("Setting {} to {} for Session object", i.name, i.value);
        s->set_value(i);
    }

    return s;
}


std::vector<std::shared_ptr<Session>> Session::filter(const std::string &filter) {
    std::vector<std::shared_ptr<Session>> out;

    SPDLOG_DEBUG("Filtering Sessioons with \"{}\"", filter);

    if (!Session::check_table()) {
        return out;
    }

    std::vector<std::vector<SqlValPair>> filtered = Model::filter("sessions", filter);
    SPDLOG_DEBUG("Found {} Sessions", filtered.size());

    out.reserve(filtered.size());
    for (const std::vector<SqlValPair> &i : filtered) {
        out.emplace_back(std::make_shared<Session>(i));
    }

    return out;
}


bool Session::exists(const long& id) {
    SPDLOG_DEBUG("Checking if Note with id {} exists", id);
    return Model::exists(Session::name, std::to_string(id));
}


bool Session::exists(const std::string& id) {
    SPDLOG_DEBUG("Checking if Note with id {} exists", id);
    return Model::exists(Session::name, id);
}

void Session::set_value(const std::string &f_name, const std::string &val) {

    SPDLOG_DEBUG("Setting field {} in Session", f_name);

    std::vector<std::shared_ptr<Field>>::iterator it = std::find_if(
        this->fields.begin(),
        this->fields.end(),
        [&](std::shared_ptr<Field> obj) {
            return obj->name == f_name;
        }
    );
    if (it != this->fields.end()) {
        auto index = std::distance(this->fields.begin(), it);
        this->fields[index]->set_value(val);
        SPDLOG_DEBUG("Session field {} set to {}", f_name, val);
    } else {
        SPDLOG_ERROR("Field \"{}\" not found in Session model", f_name);
        throw std::runtime_error("Field \"" + f_name + "\" not found in Session model");
    }
}


void Session::set_value(const SqlValPair &val) {
    this->set_value(val.name, val.value);
}


std::string Session::get_name() const {
    SPDLOG_DEBUG("Getting table name for Session");
    return this->name;
}


std::shared_ptr<Field> Session::get_field(const std::string &f_name) const {
    SPDLOG_DEBUG("Getting field {} in Session", f_name);

    auto it = std::find_if(
        this->fields.begin(),
        this->fields.end(),
        [&](std::shared_ptr<Field> obj) {
            return obj->name == f_name;
        }
    );
    if (it != this->fields.end()) {
        SPDLOG_DEBUG("Field {} found in Session", f_name);
        auto index = std::distance(this->fields.begin(), it);
        return this->fields[index];
    } else {
        SPDLOG_ERROR("Field \"{}\" not found in Session model", f_name);
        throw std::runtime_error("Field \"" + f_name + "\" not found in Session model");

    }
}


std::vector<std::shared_ptr<Field>> Session::get_fields() const {
    return this->fields;
}


std::vector<std::string> Session::get_constraints() const {
    return this->constraints;
}


std::shared_ptr<Session> Session::start() {
    // Creates a new Session object, sets start_timestamp to current,
    // updates current object and/or returns initialized Session object.

    SPDLOG_DEBUG("Checking for active Session");
    if (Session::filter("active = 1").size() > 0) {
        SPDLOG_ERROR("Active Session already exists");
        throw std::runtime_error("Active Session already exists");
    }

    SPDLOG_INFO("Starting Session");
    std::shared_ptr<Session> s = std::make_shared<Session>();
    s->save();

    return s;
}


std::shared_ptr<Session> Session::start_at(const std::string &start_ts = "") {
    // Creates a new Session object, sets start_timestamp to current,
    // updates current object and/or returns initialized Session object.

    SPDLOG_DEBUG("Checking for active Session");
    if (Session::filter("active = 1").size() > 0) {
        SPDLOG_ERROR("Active Session already exists");
        throw std::runtime_error("Active Session already exists");
    }

    SPDLOG_INFO("Starting Session");
    std::shared_ptr<Session> s = std::make_shared<Session>();
    s->get_field("start_timestamp")->set_value(start_ts);
    s->save();

    return s;;
}


void Session::stop() {
    // Stop current Session object, writing stop timestamp.
    // updates current object and/or returns Session object.

    SPDLOG_DEBUG("Checking for active Session");
    if (this->get_field("active")->value != "1") {
        SPDLOG_ERROR("Stopping inactive Session");
        throw std::runtime_error("Stopping inactive Session");
    }

    SPDLOG_INFO("Stopping Session {}", this->get_field("id")->value);
    this->set_value("end_timestamp", wtt_utils::get_current_timestamp_string());
    this->set_value("active", "0");
    this->update();
}


void Session::stop_at(const std::string &stop_ts) {
    // Stop current Session object, writing stop timestamp.
    // updates current object and/or returns Session object.

    SPDLOG_DEBUG("Checking for active Session");
    if (this->get_field("active")->value != "1") {
        SPDLOG_ERROR("Stopping inactive Session");
        throw std::runtime_error("Stopping inactive session");
    }

    SPDLOG_INFO("Stopping Session {}", this->get_field("id")->value);
    if (stop_ts.empty()) {
        this->stop();
    } else {
        this->set_value("end_timestamp", stop_ts);
        this->set_value("active", "0");
        this->update();
    }
}


std::shared_ptr<Session> Session::create_at(const std::string &start_ts, const std::string &stop_ts) {
    SPDLOG_INFO("Creating Session");
    std::shared_ptr<Session> s = std::make_shared<Session>();
    s->get_field("start_timestamp")->set_value(start_ts);
    s->get_field("end_timestamp")->set_value(stop_ts);
    s->save();

    return s;
}


std::shared_ptr<Session> Session::get_active_session() {
    SPDLOG_DEBUG("Getting active Session");

    std::vector<std::shared_ptr<Session>> filtered_sessions;

    if (Session::check_table()) {
        filtered_sessions = Session::filter("active = 1");
    }

    switch (filtered_sessions.size()) {
    case 0:
        SPDLOG_ERROR("No active Session found");
        return nullptr;
    case 1:
        return filtered_sessions[0];
    default:
        SPDLOG_ERROR("Multiple active Sessions found");
        throw std::runtime_error("Multiple active Sessions found");
    }
}


void Session::pause(const std::string &start_ts) {
    // TODO: consider if pausing an inactive session is a desired behavior

    if (this->get_field("paused")->value == "1") {
        SPDLOG_ERROR("Session already paused");
        throw std::runtime_error("Session already paused");
    }

    SPDLOG_INFO("Pausing Session {}", this->get_field("id")->value);
    if (start_ts.empty()) {
        this->set_value("pause_start_timestamp", wtt_utils::get_current_timestamp_string());
        this->set_value("paused", "1");
    } else {
        this->set_value("pause_start_timestamp", start_ts);
        this->set_value("paused", "1");
    }
    this->update();
}


void Session::unpause(const std::string &end_ts) {

    if (this->get_field("paused")->value == "0") {
        SPDLOG_ERROR("Can't unpause, Session not paused");
        throw std::runtime_error("Can't unpause, Session not paused");
    }

    SPDLOG_INFO("Unpausing Session {}", this->get_field("id")->value);
    if (end_ts.empty()) {
        int64_t curr_timestamp = wtt_utils::get_current_timestamp();
        double start_timestamp = std::stod(this->get_field("pause_start_timestamp")->value);
        double pause_duration = std::stod(this->get_field("pause")->value) + (curr_timestamp - start_timestamp);
        if (pause_duration < 0) {
            SPDLOG_ERROR("Session pause duration can't be negative");
            throw std::runtime_error("Session pause duration can't be negative");
        }
        this->set_value("pause", std::to_string(pause_duration));
        this->set_value("pause_start_timestamp", "NULL");
        this->set_value("paused", "0");
    } else {
        double curr_timestamp = std::stod(end_ts);
        double start_timestamp = std::stod(this->get_field("pause_start_timestamp")->value);
        double pause_duration = std::stod(this->get_field("pause")->value) + (curr_timestamp - start_timestamp);
        if (pause_duration < 0) {
            SPDLOG_ERROR("Session pause duration can't be negative");
            throw std::runtime_error("Session pause duration can't be negative");
        }
        this->set_value("pause", std::to_string(pause_duration));
        this->set_value("pause_start_timestamp", "NULL");
        this->set_value("paused", "0");
    }
    this->update();
}


std::ostream& operator<<(std::ostream& os, const Session& session) {
    int64_t end_timestamp;
    if (session.get_field("active")->value == "0") {
        end_timestamp = std::stoll(session.get_field("end_timestamp")->value);
    } else {
        end_timestamp = wtt_utils::get_current_timestamp();
    }

    std::string st = session.get_field("start_timestamp")->value;
    int64_t start_timestamp = std::stoi(session.get_field("start_timestamp")->value);
    int64_t time = end_timestamp - start_timestamp;
    int64_t pause_time = std::stoll(session.get_field("pause")->value);
    if (session.get_field("paused")->value == "1") {
        double start_timestamp = std::stod(session.get_field("pause_start_timestamp")->value);
        pause_time += end_timestamp - start_timestamp;
    }
    int64_t time_w_p = time - pause_time;
    int64_t h{0}, m{0}, s{0};

    os << std::left << std::setw(20) << "Session:" << session.get_field("id")->value << std::endl;

    if (session.get_field("paused")->value == "1") {
        os << std::left << "Session paused ⌛️" << std::endl;
    }

    h = time / 3600;
    time = time % 3600;
    m = time / 60;
    time = time % 60;
    s = time;
    os
        << std::left << std::setw(20) << "Overall time:"
        << std::right << h << ":"
        << std::right << m << ":"
        << std::right << s << std::endl;

    if (session.get_field("pause")->value != "0.0") {
        h = pause_time / 3600;
        pause_time = pause_time % 3600;
        m = pause_time / 60;
        pause_time = pause_time % 60;
        s = pause_time;
        os
            << std::left << std::setw(20) << "Pause:"  // NOTE: consider cahnging to "Pause time:"
            << std::right << h << ":"
            << std::right << m << ":"
            << std::right << s << std::endl;
    }

    h = time_w_p / 3600;
    time_w_p = time_w_p % 3600;
    m = time_w_p / 60;
    time_w_p = time_w_p % 60;
    s = time_w_p;
    os
        << std::left << std::setw(20) << "Work time:"
        << std::right << h << ":"
        << std::right << m << ":"
        << std::right << s;

    return os;
}
