#include <stdexcept>
#include <string>
#include <vector>
#include <memory>

#include "spdlog/spdlog.h"

#include "wtt_models/LinkSessionTask.hpp"
#include "wtt_models/Session.hpp"
#include "wtt_models/Task.hpp"
#include "wtt_core/Model.hpp"
#include "conf.h"


bool LinkSessionTask::table_created = false;


bool LinkSessionTask::check_table() {
    return table_created ? table_created : Model::table_exists(LinkSessionTask::name);
}


LinkSessionTask::LinkSessionTask() {
    table_created = LinkSessionTask::check_table();
    if (!table_created) {
        SPDLOG_DEBUG("Creating Linking table for Session(s) and Task(s) from constructor");
        Model::create_table(this->name, this->fields);
        table_created = Model::table_exists(this->name);
    }

    SPDLOG_DEBUG("Linking object for Session(s) and Task(s) created");
}

LinkSessionTask::LinkSessionTask(const std::vector<SqlValPair> &f_vals) {
    table_created = LinkSessionTask::check_table();
    if (!table_created) {
        SPDLOG_DEBUG("Creating Linking table for Session(s) and Task(s) from constructor");
        Model::create_table(this->name, this->fields);
        table_created = Model::table_exists(this->name);
    }

    SPDLOG_DEBUG("Linking object for Session(s) and Task(s) created");

    SPDLOG_DEBUG("Setting {} \"Link for Session(s) and Task(s)\" values", f_vals.size());
    for (const SqlValPair & i : f_vals) {
        SPDLOG_DEBUG("Setting {} to {}", i.name, i.value);
        this->set_value(i);
    }
}


LinkSessionTask::~LinkSessionTask() {
    SPDLOG_DEBUG("Linking object for Session(s) and Task(s) destroyed");
}


std::vector<std::shared_ptr<LinkSessionTask>> LinkSessionTask::filter(const std::string &filter) {
    std::vector<std::shared_ptr<LinkSessionTask>> out;

    SPDLOG_DEBUG("Filtering Links for Session(s) and Task(s) with \"{}\"", filter);

    if (!LinkSessionTask::check_table()) {
        return out;
    }

    std::vector<std::vector<SqlValPair>> filtered = Model::filter("sessions_tasks", filter);
    SPDLOG_DEBUG("Found {} Links for Session(s) and Task(s)", filter.size());

    out.reserve(filtered.size());

    for (const std::vector<SqlValPair> &i : filtered) {
        out.emplace_back(std::make_shared<LinkSessionTask>(i));
    }

    return out;
}


template<class T>
std::vector<std::shared_ptr<T>> LinkSessionTask::get_all_linked_to(const std::string &selector_name, const std::string &selector_id) {
    if (selector_name != "task" && selector_name != "session") {
        throw std::runtime_error("Selector is not part of Linking table");
    }


    SPDLOG_DEBUG("Getting all Links for Session(s) and Task(s) telated to \"{}\" with id {}", selector_name, selector_id);
    std::vector<std::shared_ptr<LinkSessionTask>>links = LinkSessionTask::filter(selector_name + "_id = " + selector_id);
    SPDLOG_DEBUG("Found {} Links for Session(s) and Task(s)", links.size());

    std::vector<std::shared_ptr<T>> out;
    out.reserve(links.size());

    for (auto link : links) {
        std::string linked_id = link->get_field((selector_name == "task") ? "session_id" : "task_id")->value;
        out.push_back(T::get(linked_id));
    }

    return out;
}
template std::vector<std::shared_ptr<Task>> LinkSessionTask::get_all_linked_to(const std::string&, const std::string&);
template std::vector<std::shared_ptr<Session>> LinkSessionTask::get_all_linked_to(const std::string&, const std::string&);


template<class T>
std::vector<std::shared_ptr<T>> LinkSessionTask::get_all_linked_to(const std::string &selector_name, const long &selector_id) {
    return LinkSessionTask::get_all_linked_to<T>(selector_name, std::to_string(selector_id));
}
template std::vector<std::shared_ptr<Task>> LinkSessionTask::get_all_linked_to(const std::string&, const long&);
template std::vector<std::shared_ptr<Session>> LinkSessionTask::get_all_linked_to(const std::string&, const long&);


void LinkSessionTask::set_value(const std::string &f_name, const std::string &val) {

    SPDLOG_DEBUG("Setting field {} in Links for Session(s) and Task(s)", f_name);

    std::vector<std::shared_ptr<Field>>::iterator it = std::find_if(
        this->fields.begin(),
        this->fields.end(),
        [&](std::shared_ptr<Field> obj) {
            return obj->name == f_name;
        }
    );

    if (it != this->fields.end()) {
        auto index = std::distance(this->fields.begin(), it);
        this->fields[index]->set_value(val);
        SPDLOG_DEBUG("Link for Session(s) and Task(s) field {} set to {}", f_name, val);
    } else {
        SPDLOG_ERROR("Field \"{}\" not found in Link for Session(s) and Task(s) model", f_name);
        throw std::runtime_error("Field \"" + f_name + "\" not found in Link for Session(s) and Task(s) model");
    }

    SPDLOG_DEBUG("Field {} set in Links for Session(s) and Task(s)", f_name);
}


void LinkSessionTask::set_value(const SqlValPair &val) {
    this->set_value(val.name, val.value);
}


std::string LinkSessionTask::get_name() const {
    SPDLOG_DEBUG("Getting table name for Links for Session(s) and Task(s)");
    return this->name;
}


std::shared_ptr<Field> LinkSessionTask::get_field(const std::string &f_name) const {
    SPDLOG_DEBUG("Getting field {} in Link for Session(s) and Task(s)", f_name);

    auto it = std::find_if(
        this->fields.begin(),
        this->fields.end(),
        [&](std::shared_ptr<Field> obj) {
            return obj->name == f_name;
        }
    );

    if (it != this->fields.end()) {
        SPDLOG_DEBUG("Field {} found in Links fo Session(s) and Task(s)", f_name);
        auto index = std::distance(this->fields.begin(), it);
        return this->fields[index];
    } else {
        SPDLOG_ERROR("Field \"{}\" not found in Link for Session(s) and Task(s) model", f_name);
        throw std::runtime_error("Field \"" + f_name +  "\" not found in Link for Session(s) and Task(s) model");

    }
}


std::vector<std::shared_ptr<Field>> LinkSessionTask::get_fields() const {
    SPDLOG_DEBUG("Getting fields for Links for Session(s) and Task(s) model");
    return this->fields;
}


std::vector<std::string> LinkSessionTask::get_constraints() const {
    SPDLOG_DEBUG("Getting constraints for Links for Session(s) and Task(s) model");
    return this->constraints;
}

