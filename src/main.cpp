#include "wtt_app/app.hpp"

int main(int argc, char **argv) {
    wtt_app::App *app = new wtt_app::App();
    app->run(argc, argv);
    delete app;

    return 0;
}
