#include <vector>
#include <string>

#include "CLI/CLI.hpp"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"

#include "wtt_utils/utils.hpp"

#include "wtt_models/Task.hpp"
#include "wtt_models/Note.hpp"
#include "wtt_models/Session.hpp"

#include "conf.h"


/* ===== UTILITIES ===== */
std::string wtt_utils::get_current_timestamp_string() {
    return std::to_string(std::chrono::duration_cast<std::chrono::seconds>(
        std::chrono::system_clock::now().time_since_epoch()
    ).count());
}


int64_t wtt_utils::get_current_timestamp() {
    return std::chrono::duration_cast<std::chrono::seconds>(
        std::chrono::system_clock::now().time_since_epoch()
    ).count();
}


std::ostringstream wtt_utils::text_from_editor() {
    std::ostringstream editor_call;
    std::ostringstream editor_text;
    editor_call << "\"" << EDITOR << "\" \"" << NOTEFILE << "\"";

    std::system(editor_call.str().c_str());

    std::string line;
    std::ifstream read_temp_file(NOTEFILE);
    while (getline(read_temp_file, line)) {
        editor_text << line << std::endl;
    }
    return editor_text;
}


std::string wtt_utils::stringsafe(const std::string &text) {
    // TODO: write a true base64 encoder or int vector here
    static const std::string b = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";//=

    std::string out;

    int val=0, valb=-6;
    for (unsigned char c : text) {
        val = (val<<8) + c;
        valb += 8;
        while (valb>=0) {
            out.push_back(b[(val>>valb)&0x3F]);
            valb-=6;
        }
    }
    if (valb>-6) out.push_back(b[((val<<8)>>(valb+8))&0x3F]);
    while (out.size()%4) out.push_back('=');
    return out;
}

std::string wtt_utils::strdecode(const std::string &in) {

    std::string out;

    std::vector<int> T(256,-1);
    for (int i=0; i<64; i++) T["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[i]] = i;

    int val=0, valb=-8;
    for (unsigned char c : in) {
        if (T[c] == -1) break;
        val = (val << 6) + T[c];
        valb += 6;
        if (valb >= 0) {
            out.push_back(char((val>>valb)&0xFF));
            valb -= 8;
        }
    }
    return out;
}
