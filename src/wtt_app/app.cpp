#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <vector>


#include "CLI/App.hpp"
#include "CLI/Option.hpp"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/rotating_file_sink.h"


#include "wtt_app/app.hpp"
#include "wtt_models/Session.hpp"
#include "wtt_models/Note.hpp"
#include "wtt_models/Task.hpp"
#include "wtt_models/LinkSessionTask.hpp"
#include "wtt_utils/utils.hpp"



wtt_app::App::App(){
    // Init parts of wtt app cli
    this->init_logger();
    this->init_cli();
    this->init_session_cli();
    this->init_note_cli();
    this->init_task_cli();
}


wtt_app::App::~App(){
}


// Hemper method to run wtt cli app
int wtt_app::App::run(int argc, char **argv) {

    SPDLOG_DEBUG("APP STARTED");

    std::cout
        << " __     __        ______      ______  " << std::endl
        << "/\\ \\  _ \\ \\      /\\__  _\\    /\\__  _\\ " << std::endl
        << "\\ \\ \\/ \".\\ \\     \\/_/\\ \\/    \\/_/\\ \\/ " << std::endl
        << " \\ \\__/\".~\\_\\       \\ \\_\\       \\ \\_\\ " << std::endl
        << "  \\/_/   \\/_/        \\/_/        \\/_/ " << std::endl << std::endl;

    Model::init_db();
    CLI11_PARSE(*this->cli_app, argc, argv);
    Model::close_db();

    SPDLOG_DEBUG("APP ENDED");

    return 0;
}


// Helper method to init logger (spdlog)
void wtt_app::App::init_logger() {
    try {
        // auto logger = spdlog::default_logger();
        auto logger = spdlog::rotating_logger_mt(
            "basic_logger", LOGFILE, this->log_max_size, 1
        );
        spdlog::set_default_logger(logger);
    }
    catch (const spdlog::spdlog_ex &ex) {
        std::cout << "Log initiation failed: " << ex.what() << std::endl;
    }

    spdlog::set_pattern(LOG_PATTERN);
    spdlog::set_level(LOG_LEVEL);
    SPDLOG_DEBUG("LOGGING INITIALIZED");
}


// Helper method to init cli part (CLI11)
void wtt_app::App::init_cli() {
    this->cli_app = std::make_shared<CLI::App>("Work Time Tracker");
    this->cli_app->set_help_all_flag("--help-all");

    this->cli_app->require_subcommand(/* min */ 1, /* max */ 1);
    SPDLOG_DEBUG("CLI INITIALIZED");
}


// Session
void wtt_app::App::init_session_cli() {

    SPDLOG_DEBUG("STARTED SESSION CLI");

    // Main Session subcommand
    CLI::App *session = this->cli_app->add_subcommand(
        "session", "W.T.T. Session"
    );

    session->immediate_callback(true);

    // TODO: refactor with dynamic output creation
    session->callback([session]() {
        CLI::Option *list     = session->get_option("--list-all");
        CLI::Option *start    = session->get_option("--start");
        CLI::Option *end      = session->get_option("--end");
        CLI::Option *note_ids = session->get_option("--note-ids");
        CLI::Option *notes    = session->get_option("--notes");


        if (session->parsed()) { // TODO: add session output fitered by id

            // Session listing (session -l)
            if (list->count() > 0) {

                SPDLOG_DEBUG("Listing Sessions");

                // Get all available sessions
                std::vector<std::shared_ptr<Session>> sessions = Session::filter();
                if (sessions.empty()) {
                    SPDLOG_DEBUG("No Sessions found to list");
                    std::cout << "No sessions found" << std::endl;
                    return;
                }

                // Session time buffers
                tm buff_sltm;
                tm buff_eltm;

                for (auto session : sessions) {

                    // Get staring time for session object and adjust it
                    time_t st = std::stoll(session->get_field("start_timestamp")->value);
                    struct tm sltm;
                    localtime_r(&st, &sltm);

                    // Show active session if found else add ending time with relevant info
                    if (session->get_field("active")->value == "1") {
                        std::cout << std::endl << "[ACTIVE]===========================" << std::endl;
                    } else {
                        // TODO: fix same date session listing
                        // Get ending time for session object and adjust it
                        time_t et = std::stoll(session->get_field("end_timestamp")->value);
                        struct tm eltm;
                        localtime_r(&et, &eltm);

                        // NOTE: Ugly sollution for getting time-frames
                        if (
                            (eltm.tm_year > sltm.tm_year || eltm.tm_yday > sltm.tm_yday)
                            &&
                            (
                                (eltm.tm_year != buff_eltm.tm_year && eltm.tm_yday != buff_eltm.tm_yday)
                                ||
                                (sltm.tm_year != buff_sltm.tm_year && sltm.tm_yday != buff_sltm.tm_yday)
                            )
                        ) {
                            std::cout << std::endl
                                << "[" << sltm.tm_mday << "/" << sltm.tm_mon + 1 << "/" << sltm.tm_year + 1900
                                << " - "
                                << eltm.tm_mday << "/" << eltm.tm_mon + 1 << "/" << eltm.tm_year + 1900
                                << "]==============" << std::endl;
                            buff_eltm = eltm;
                            buff_sltm = sltm;
                        } else if (sltm.tm_year == eltm.tm_year && sltm.tm_yday == eltm.tm_yday) {
                            std::cout << std::endl
                                      << "[" << sltm.tm_mday << "/" << sltm.tm_mon + 1 << "/" << sltm.tm_year + 1900
                                      << "]=========================" << std::endl;
                            buff_eltm = eltm;
                            buff_sltm = sltm;
                        }
                    }

                    // Print relevant session information
                    std::cout << std::endl;
                    std::cout << *session << std::endl;
                }

                SPDLOG_DEBUG("Listed {} Sessions", sessions.size());
                return;
            }

            // Get current active session for printout
            if (start->count() == 0 && end->count() == 0) {
                SPDLOG_DEBUG("Printing current Session");
                std::shared_ptr<Session> as = wtt_app::get_active_session();
                std::cout << *as << std::endl;
                if (note_ids->count() > 0 || notes->count() > 0) {
                    wtt_app::print_related_notes("session_id", as->get_field("id")->value, notes->count() > 0);
                }
            }
        }
    });

    if (session) {
        // Declare session options and flags
        auto a  = session->add_flag(
            "-l,--list-all",
            this->session__list_flag,
            "Display all sessions"
        );
        auto n  = session->add_flag(
            "-n,--notes",
            this->session__list_linked_notes_flag,
            "List linked Notes"
        );
        auto ni = session->add_flag(
            "--note-ids",
            this->session__list_linked_note_ids_flag,
            "List linked Note IDs"
        );

        // Session start (session -s)
        auto s = session->add_option_function<std::string>(
            "-s,--start",
            wtt_app::start_session,
            "Start timestamp"
        )->expected(0, 1);

        // Session end (session -e)
        auto e = session->add_option_function<std::string>(
            "-e,--end",
            wtt_app::end_session,
            "End Timestamp"
        )->expected(0, 1);

        // TODO: add pause/pause_at as flag/option
        // TODO: add unpause/unpause_at as flag/option
        // TODO: add link/unlink task(s) as options
        // TODO: add link/unlink session as option

        // Flags/options relationships declarations
        s->excludes(a, e);
        e->excludes(a, s);
        a->excludes(
            s, e, n, ni
        );
    }

    // Start
    CLI::App *start = this->cli_app->add_subcommand("start", "W.T.T. Session Start");
    start->immediate_callback(true);

    // Start: noparam
    start->callback([start]() {
        CLI::Option *ts = start->get_option("--timestamp");

        // Start session at current timestamp
        if (ts->count() == 0) {
            wtt_app::start_session();
        }
    });

    if (start) {
        // Start: -t (start -t)
        start->add_option_function<std::string>(
            "-t,--timestamp",
            wtt_app::start_session,
            "Start timestamp"
        );
    }

    // Stop
    CLI::App *end = this->cli_app->add_subcommand("end", "W.T.T. Session End");
    end->immediate_callback(true);

    // Stop: noparam
    end->callback([end]() {
        CLI::Option *ts = end->get_option("--timestamp");

        // End session at current timestamp
        if (ts->count() == 0) {
            wtt_app::end_session();
        }
    });

    if (end) {
        // End session with timestamp (end -t)
        end->add_option_function<std::string>(
            "-t,--timestamp",
            wtt_app::end_session,
            "Start timestamp"
        );
    }

    // Pause
    CLI::App *pause = this->cli_app->add_subcommand("pause", "Add Pause to W.T.T. Session");
    pause->immediate_callback(true);

    // 
    pause->callback([pause]() {
        CLI::Option *ts = pause->get_option("--timestamp");

        // Pause session at current timestamp
        if (ts->count() == 0) {
            wtt_app::pause_session();
        }
    });

    if (pause) {
        // Pause session with timestamp (pause -t)
        pause->add_option_function<std::string>(
            "-t,--timestamp",
            wtt_app::pause_session,
            "Pause start timestamp"
        );
    }

    // Unpause
    CLI::App *unpause = this->cli_app->add_subcommand("unpause", "Unpause curretly paused W.T.T. Session");

    unpause->immediate_callback(true);
    unpause->callback([unpause]() {
        CLI::Option *ts = unpause->get_option("--timestamp");

        // Unause session at current timestamp
        if (ts->count() == 0) {
            wtt_app::unpause_session();
        }
    });

    if (unpause) {
        // Unpause session with timestamp (unpause -t)
        unpause->add_option_function<std::string>(
            "-t,--timestamp",
            wtt_app::unpause_session,
            "Pause start timestamp"
        );
    }
}


// Note
void wtt_app::App::init_note_cli() {

    // Main Note subcommand
    CLI::App *note = this->cli_app->add_subcommand(std::string("note"), std::string("W.T.T. Note"));

    note->immediate_callback(true);

    // TODO: refactor with dynamic output creation
    note->callback([this, note](){
        CLI::Option *list     = note->get_option("--list");
        CLI::Option *contents = note->get_option("--contents");
        CLI::Option *text     = note->get_option("--text");
        CLI::Option *id       = note->get_option("--id");
        CLI::Option *edit     = note->get_option("--edit");

        std::shared_ptr<Note> note_instance;

        if (note->parsed()) {
            // Note listing (note -l)
            if (list->count() > 0) {
                std::vector<std::shared_ptr<Note>> notes = Note::filter("");
                for (auto n : notes) {
                    std::cout << *n << std::endl;
                    if (contents->count() > 0) {
                        std::cout << wtt_utils::strdecode(n->get_field("text")->value) << std::endl;
                    }
                }
                return;
            }

            // Note by id (note -i)
            if (id->count() > 0) {
                try {
                    note_instance = Note::get(this->note__id);
                } catch (std::runtime_error &err) {
                    std::cout << err.what() << std::endl;
                    std::exit(EXIT_FAILURE);
                }
                std:: cout << *note_instance << std::endl << std::endl;
                if (contents->count() > 0) {
                    std::cout << wtt_utils::strdecode(note_instance->get_field("text")->value) << std::endl;
                }
            } else if (this->note__id > 0 && id->count() == 0) {  // Note created in previous callback
                note_instance = Note::get(this->note__id);
            } else {  // Note id wasn't provided and wasn't created in previous callbacks
                note_instance = std::make_shared<Note>();
            }

            // Check if listing or showing by id, when contents flag (-c) is passed
            if (contents->count() > 0 && (list->count() == 0 && id->count() == 0)) {
                std::cout << "Please provide --list flag or --id flag with note id" << std::endl;
                std::exit(EXIT_FAILURE);
            }

            // Creating new note or editing existing one
            if ((id->count() == 0 || edit->count() > 0) || (this->note__id > 0 && id->count() == 0)) {
                // Final output buffer
                std::ostringstream note_text;

                //TODO: possibly move to option function
                // Get note text from option or ask for note text input
                if (text->count() > 0) {
                    note_text << this->note__text;
                } else {
                    note_text = wtt_utils::text_from_editor();
                }

                // Check note text
                if (note_text.str().empty()) {
                    std::cout << "Note text can't be empty." << std::endl;
                    return;
                }

                // Encode string of note text to save
                std::string out = wtt_utils::stringsafe(note_text.str());

                // Print out note text before saving
                std::cout << note_text.str() << std::endl;
                std::cout << out << std::endl; // NOTE: debug

                // Save note text
                note_instance->get_field("text")->set_value(out);
                note_instance->save();

                // Cleanup after editor input
                std::remove(NOTEFILE);
            }
        }
    });

    if (note) {
        // Declare note options and flags
        auto i = note->add_option(
            "-i,--id",
            this->note__id,
            "Note ID"
        );
        auto t = note->add_option(
            "-t,--text",
            this->note__text,
            "Pass text for note"
        );
        auto l = note->add_flag(
            "-l,--list",
            this->note__list_flag,
            "List notes"
        );
        auto c = note->add_flag(
            "-c,--contents",
            this->note__cnotents_flag,
            "Show note text"
        ); // NOTE: always show contents
        auto e = note->add_flag(
            "-e,--edit",
            this->note__edit_flag,
            "Edit existing note"
        );

        // Link note (can create note first) with session
        auto sl = note->add_option_function<long>(
            "--session",
            [this, note](long session_id) {
                this->note__id = wtt_app::single_link<Note, Session>(
                    this->note__id,
                    session_id,
                    "session_id"
                );
            },
            "Link note with session"
        );

        // Unlink note from session
        auto us = note->add_flag_callback(
            "--unlink-session",
            [this, note]() {
                wtt_app::single_unlink<Note>(
                    this->note__id,
                    "session_id"
                );
            },
            "Unlink note from session"
        );


        // Link note (can create note first) with task
        auto tl = note->add_option_function<long>(
            "--task",
            [this, note](long task_id) {
                this->note__id = wtt_app::single_link<Note, Task>(
                    this->note__id,
                    task_id,
                    "task_id"
                );
            },
            "Link note with task"
        );

        // Unlink note from task
        auto ut = note->add_flag_callback(
            "--unlink-task",
            [this, note]() {
                wtt_app::single_unlink<Note>(
                    this->note__id,
                    "task_id"
                );
            },
            "Unlink note from task"
        );

        // Unlink note from all tasks and sessions it is connected to
        auto u = note->add_flag_callback(
            "-u,--unlink-all",
            [this, note]() {
                wtt_app::single_unlink<Note>(
                    this->note__id,
                    "session_id"
                );
                wtt_app::single_unlink<Note>(
                    this->note__id,
                    "task_id"
                );
            },
            "Unlink task and session from note"
        );


        // Flags/options relationships declarations
        sl->excludes(
            u, us, ut
        );
        tl->excludes(
            u, us, ut
        );
        c->excludes(
            t, sl, u, us, ut
        );

        l->excludes(
            t, sl, u, us, ut, i, e
        );

        u->needs(i);
        us->needs(i);
        ut->needs(i);
        e->needs(i);
    }
}


// Task
void wtt_app::App::init_task_cli() {
    // Main Note subcommand
    CLI::App *task = this->cli_app->add_subcommand(std::string("task"), std::string("W.T.T. Task"));

    task->immediate_callback(true);

    // TODO: refactor
    task->callback([this, task](){
        CLI::Option *id          = task->get_option("--id");
        CLI::Option *list        = task->get_option("--list");
        CLI::Option *note_ids    = task->get_option("--note-ids");
        CLI::Option *notes       = task->get_option("--notes");
        CLI::Option *description = task->get_option("--description");
        CLI::Option *edit        = task->get_option("--edit");
        // CLI::Option *edit_description   = task->get_option("--edit-summary");  // NOTE: might be useful in future
        // CLI::Option *edit_summary       = task->get_option("--edit-description");  // NOTE: might be useful in future

        std::shared_ptr<Task> task_instance;

        if (task->parsed()) {
            // Task listing (task -l)
            if (list->count() > 0) {
                // Get all tasks
                std::vector<std::shared_ptr<Task>> tasks = Task::filter("");
                // Print task
                for (auto t : tasks) {
                    std::cout << *t << std::endl;
                    // Priont descriprion if relevant flag was passed
                    if (description->count() > 0) {
                        std::cout << "Description:" << std::endl;
                        std::cout << wtt_utils::strdecode(
                            t->get_field("description")->value
                        ) << std::endl;
                    }
                }
                std::cout << std::endl;
                return;
            }

            // Task by id (task -i)
            if (id->count() > 0) {
                // Get task by id
                try {
                    task_instance = Task::get(this->task__id);
                } catch (std::runtime_error &err) {
                    std::cout << err.what() << std::endl;
                    std::exit(EXIT_FAILURE);
                }
                // Print task
                std:: cout << *task_instance << std::endl;  // TODO: Add std output for task type

                // Print description if relevant flag was passed
                if (description->count() > 0) {
                    std::cout << "Description:" << std::endl;
                    std::cout << wtt_utils::strdecode(
                        task_instance->get_field("description")->value
                    ) << std::endl;
                }

                // Print linked notes/note-ids if relevant flag was passed
                if (note_ids->count() > 0 || notes->count() > 0) {
                    wtt_app::print_related_notes("task_id", this->task__id, notes->count() > 0);
                }
                std::cout << std::endl;
            } else if (this->task__id > 0) {  // Get task if created in previous callback
                task_instance = Task::get(this->task__id);
            } else {  // Create task if not selected by id and not created in previous callbacks
                task_instance = std::make_shared<Task>();
            }

            // Handle describe falg if task id or list flags were not provided
            if (description->count() > 0 && (list->count() == 0 && id->count() == 0)) {
                std::cout << "Please provide --list flag or --id option with task id" << std::endl;
            }

            // Handle notes flag if task id or list flag were not provided
            if (note_ids->count() > 0 && (list->count() == 0 && id->count() == 0)) {
                std::cout << "Please provide --list flag or --id option with task id" << std::endl;
            }

            // Handle notes flag if task id or list flag were not provided
            if (notes->count() > 0 && (list->count() == 0 && id->count() == 0)) {
                std::cout << "Please provide --list flag or --id option with task id" << std::endl;
            }

            // Creating new task or editing existing one
            if (id->count() == 0 || edit->count() > 0) {

                // Get summary and description for task
                for (std::string editing_part : {"summary", "description"}) {
                    reask_for_input:  // Label to handle jumps on invalid input

                    std::string q_input = "y"; // Declare input buffer with default value

                    // Ask for input
                    std::cout << "Do you want to "
                              << ((edit->count() > 0) ? "edit" : "enter")
                              << " the " << editing_part << "? [Y/n]: ";
                    std::getline(std::cin, q_input);

                    // Check input
                    if (q_input == "n" || q_input == "N") {  // Handle negative input
                        continue;
                    } else if (q_input != "y" && q_input != "Y" && q_input != "") {  // Handle invalid input
                        goto reask_for_input;
                    }

                    // Read text for dumary and description
                    std::ostringstream in_text;
                    in_text = wtt_utils::text_from_editor();

                    // Check task summary text
                    if (in_text.str().empty() && editing_part == "summary") {
                        std::cout << "Summary can't be empty." << std::endl;
                        return;
                    }

                    // Encode task summary/description text
                    std::string out = wtt_utils::stringsafe(in_text.str());

                    // Priont out task summary/description text
                    std::cout << in_text.str() << std::endl;
                    std::cout << out << std::endl; // NOTE: debug

                    // Save task summary/description
                    task_instance->get_field(editing_part)->set_value(out);
                    task_instance->save();

                    // Cleanup after editor input
                    std::remove(NOTEFILE);
                }
            }
        }
    });

    if (task) {
        // Declare task options and flags
        auto i = task->add_option(
            "-i,--id",
            this->task__id,
            "Task ID"
        );
        auto l = task->add_flag(
            "-l,--list",
            this->task__list_flag,
            "List tasks"
        );
        auto n = task->add_flag(
            "-n,--notes",
            this->task__list_linked_notes_flag,
            "List linked Notes"
        );
        auto ni = task->add_flag(
            "--note-ids",
            this->task__list_linked_note_ids_flag,
            "List linked Note IDs"
        );
        auto d = task->add_flag(
            "-d,--description",
            this->task__description_flag,
            "Show Task Description"
        );
        auto e = task->add_flag(
            "-e,--edit",
            this->task__edit_flag,
            "Edit existing task"
        );

        // Change status for task or create task wit status
        auto s  = task->add_option_function<std::string>(
            "-s,--status",
            [this, task](std::string status) {
                // Get task by id or create a new task
                std::shared_ptr<Task> task_instance = wtt_app::get_related_object<Task>(this->task__id);

                // Set status and save
                task_instance->get_field("status")->set_value(status);
                task_instance->save();

                // Set id if new task was created
                this->task__id = std::stoll(task_instance->get_field("id")->value);
            },
            "Change status value of a Task"
        );

        // Link task (can be created first) with sessions
        auto sl = task->add_option_function<std::vector<long>>(
            "--link-sessions",
            [this, task](std::vector<long> session_ids) {
                // Get task by id or create a new task
                std::shared_ptr<Task> task_instance = wtt_app::get_related_object<Task>(this->task__id);
                this->task__id = std::stoll(task_instance->get_field("id")->value);

                // Link task with sessions from list
                for (auto session_id : session_ids) {
                    wtt_app::mtm_link<LinkSessionTask, Task, Session> (
                        {"task_id", this->task__id},
                        {"session_id", session_id}
                    );
                }
            },
            "Link Task with Sessionsos create a Task linked with Sessions"
        );

        // Link task (can ce vreated first) with session
        auto osl = task->add_option_function<long>(
            "--link-session",
            [this, task](long session_id) {
                // Get task by id or create new task
                std::shared_ptr<Task> task_instance = wtt_app::get_related_object<Task>(this->task__id);
                this->task__id = std::stoll(task_instance->get_field("id")->value);

                // Link task with a session
                wtt_app::mtm_link<LinkSessionTask, Task, Session> (
                    {"task_id", this->task__id},
                    {"session_id", session_id}
                );
            },
            "Link Task with a Session or create a Task in a Session"
        );

        // Link task (can be created first) with an active session
        auto csl = task->add_flag_callback(
            "--link-active-session",
            [this, task]() {
                // Get active session
                std::shared_ptr<Session> active_session = wtt_app::get_active_session();

                // Get task by id or create a new task
                std::shared_ptr<Task> task_instance = wtt_app::get_related_object<Task>(this->task__id);
                this->task__id = std::stoll(task_instance->get_field("id")->value);

                // Link app with a session
                wtt_app::mtm_link<LinkSessionTask, Task, Session> (
                    {"task_id", this->task__id},
                    {
                        "session_id",
                        std::stoll(active_session->get_field("id")->value)
                    }
                );
            },
            "Link Task with an active Session or create a Task in an active Session"
        );

        // Unlink task from sessions
        auto us = task->add_option_function<std::vector<long>>(
            "--unlink-sessions",
            [this](std::vector<long> session_ids) {
                for ( auto session_id : session_ids) {
                    wtt_app::mtm_unlink<LinkSessionTask>(
                        {"task_id", this->task__id},
                        {"session_id", session_id}
                    );
                }
            },
            "Unlink Sessions from Task"
        );

        // Unlink task from a session
        auto uos = task->add_option_function<long>(
            "--unlink-session",
            [this](long session_id) {
                wtt_app::mtm_unlink<LinkSessionTask>(
                    {"task_id", this->task__id},
                    {"session_id", session_id}
                );
            },
            "Unlink Session from task"
        );

        // Flags/options relationships declarations
        sl->excludes(us, uos);
        osl->excludes(us, uos);
        csl->excludes(us, uos);
        l->excludes(
            sl, osl, csl, us, uos, i, e
        );

        us->needs(i);
        uos->needs(i);
        e->needs(i);
    }
}


// Helper function to get active session or exit app appripriately
std::shared_ptr<Session> wtt_app::get_active_session() {
    SPDLOG_DEBUG("Getting active Session");

    // Get active session
    std::shared_ptr<Session> session;
    try {
        session = Session::get_active_session();
    } catch (std::runtime_error &e) {
        std::cout << e.what() << std::endl;
        SPDLOG_ERROR("[ErrorInCallback] ",  e.what());
        std::exit(EXIT_FAILURE);
    }
    // Check if active session exists
    if (!session) {
        std::cout << "No active session found" << std::endl;
        SPDLOG_ERROR("No active session found");
        std::exit(EXIT_FAILURE);
    }

    return session;
}


// Helper function to start session
void wtt_app::start_session(const std::string &start_timestamp) {
    SPDLOG_DEBUG("[subcommand/Option Callback] Starting new Session");

    // Start session
    std::shared_ptr<Session> session;
    try {
        if (start_timestamp.empty()) {  // Start at current timestamp
            session = Session::start();
        } else {  // Start at provided timestamp
            session = Session::start_at(start_timestamp);
        }
    } catch (std::runtime_error &err) {
        SPDLOG_DEBUG("[ErrorInCallback] Error starting new Session: {}", err.what());
        std::cout << err.what() <<std::endl;
        std::exit(EXIT_FAILURE);
    }

    // Print relevant info about started session
    char str[100];
    time_t st = std::stoll(session->get_field("start_timestamp")->value);
    std::strftime(str, sizeof(str), "%c", std::localtime(&st));
    std::cout << "Session started " << str << std::endl;

    SPDLOG_DEBUG("[EndCallback]");
}


// Helper function to stop session
void wtt_app::end_session(const std::string &end_timestamp) {
    SPDLOG_DEBUG("[Subcommand/Option Callback] Stopping Session");

    // Get active session
    std::shared_ptr<Session> session = wtt_app::get_active_session();

    // End session
    if (end_timestamp.empty()) {  // End at current timestamp
        // Handle paused session
        if (session->get_field("paused")->value == "1") {
            session->unpause();
        }

        // Stop
        session->stop();
    } else { // End at provided timestamp
        // Handle paused session
        if (session->get_field("paused")->value == "1") {
            session->unpause(end_timestamp);
        }

        // Stop
        session->stop_at(end_timestamp);
    }

    // Print relevant info about ended session
    char str[100];
    time_t st = std::stoll(session->get_field("start_timestamp")->value);
    std::strftime(str, sizeof(str), "%c", std::localtime(&st));
    std::cout << "Session ended " << str << std::endl;
    std::cout << std::endl << *session << std::endl;

    SPDLOG_DEBUG("[EndCallback]");
}


// Helper function to pause session
void wtt_app::pause_session(const std::string &pause_timestamp) {
    SPDLOG_DEBUG("[Subcommand/Option Callback] Pausing Session");

    // Get active session
    std::shared_ptr<Session> session = wtt_app::get_active_session();

    // Pause session
    std::string session_paused = session->get_field("paused")->value;
    if(session_paused == "0") {  // Pause session if unpaused
        if (pause_timestamp.empty()) {  // Pause at current timestamp
            session->pause();
        } else {  // Pause at provided timestamp
            session->pause(pause_timestamp);
        }

        // Print relevant info about paused session
        char str[100];
        time_t st = std::stoll(session->get_field("pause_start_timestamp")->value);
        std::strftime(str, sizeof(str), "%c", std::localtime(&st));
        std::cout << "Session paused " << str << std::endl;
    } else {  // Session already paused
        std::cout << "Session is already paused" << std::endl;
        SPDLOG_WARN("Trying to pause Session that is paused");
    }

    SPDLOG_DEBUG("[EndCallback]");
}


// Helper function to unpause session
void wtt_app::unpause_session(const std::string &unpause_timestamp) {
    SPDLOG_DEBUG("[Subcommand/Option Callback] Unpausing Session");

    // Get active session
    std::shared_ptr<Session> session = wtt_app::get_active_session();

    // Unpause session
    std::string session_paused = session->get_field("paused")->value;
    if(session_paused == "1") {  // Unpause session if paused
        if (unpause_timestamp.empty()) {  // Unpause at current timestamp
            session->unpause();
        } else {  // Unpause at provided timestamp
            session->unpause(unpause_timestamp);
        }

        // Print relevant info about unpaused session
        std::cout << "Session unpaused" << std::endl;
        std::cout << std::endl << *session << std::endl;
    } else {  // Session is not paused
        std::cout << "Session is not paused" << std::endl;
        SPDLOG_WARN("Trying unpause Session that is not paused");
    }

    SPDLOG_DEBUG("[EndCallback]");
}


// Helper function to pint related notes/note-ids
void wtt_app::print_related_notes(
    const std::string &foreign_key_name,
    const std::string &foreign_key_value,
    const bool &print_with_contents
) {
    // Get notes
    std::vector<std::shared_ptr<Note>> notes = Note::filter(
        foreign_key_name + " = " + foreign_key_value
    );
    if (!print_with_contents) {
        // If linked notes exist print linked note ids
        if (notes.size() > 0){
            std::string sep = "";
            std::cout << "Linked Note IDs: ";
            for (std::shared_ptr<Note> note : notes) {
                std::cout << sep << note->get_field("id")->value;
                sep = ", ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    } else {  // Print linked notes with note text
        // Print helper text if linked notes exist
        if (notes.size() > 0) {
            std::cout << "Linked Notes:" << std::endl;
        }

        // Print linked notes with note text
        for (std::shared_ptr<Note> note : notes) {
            std::cout << "=========================" << std::endl;
            std::cout << "> Note ID: " << note->get_field("id")->value << std::endl;
            std::cout << "> Contents:" << std::endl
                      << wtt_utils::strdecode(note->get_field("text")->value)
                      << "=========================" << std::endl;
        }
    }

}


// Helper function to pint related notes/note-ids
void wtt_app::print_related_notes(
    const std::string &foreign_key_name,
    const long &foreign_key_value,
    const bool &print_with_contents
) {
    wtt_app::print_related_notes(
        foreign_key_name,
        std::to_string(foreign_key_value),
        print_with_contents
    );
}


// Helper function to get object related to aommand (App) or subcommand (app)
template<class MT>
std::shared_ptr<MT> wtt_app::get_related_object(const long &main_id) {
    SPDLOG_DEBUG("Getting related object");

    // Instance of (Session, Note, Task)
    std::shared_ptr<MT> instance;
    if (main_id > 0) {  // Get instance by provided id
        instance = MT::get(main_id);
    } else {  // Create new instance
        instance = std::make_shared<MT>();
        instance->save();
    }

    return instance;
}


// Helper function to link two entries
template<class MT, class LT>
long wtt_app::single_link(const long &main_id, const long &link_id, const std::string &link_field_name) {
    SPDLOG_DEBUG("Linking entries");

    // Get "main" instance that holds the foreigh key
    std::shared_ptr<MT> instance;
    if (main_id > 0) {  // Get instance by provided id
        instance = MT::get(main_id);
    } else {  // Create new instance
        instance = std::make_shared<MT>();
    }

    // Connect entry by foreigh key
    if (LT::exists(link_id)) {  // Connect entry by provided id
        instance->get_field(link_field_name)->set_value(std::to_string(link_id));
        instance->save();
    } else {  // Handle connecting an unexisting entry
        std::cout << "Entry you tried to link does not exist." << std::endl;
        std::cout << std::endl;
        SPDLOG_WARN("Trying to link entry that doesn't exist");
    }

    return std::stoll(instance->get_field("id")->value);
}


// Helper function to unlink two entries
template <class MT>
void wtt_app::single_unlink(const long &main_id, const std::string &link_field_name) {
    SPDLOG_DEBUG("Unlinking entries");

    // Get "main" instance that holds foreign key
    std::shared_ptr<MT> instance;
    if (main_id > 0) {  // Get instance by provided id
        instance = MT::get(main_id);
    } else {  // Handle no "main" instance
        SPDLOG_WARN("Trying to link to an unexisting entry");
        std::exit(EXIT_FAILURE);
    }

    // Unlink entries
    instance->get_field(link_field_name)->set_value("");
    instance->save();
}


// Helper function to link entries in m2m relationship
template<class MT, class FLT, class SLT>
void wtt_app::mtm_link(const wtt_app::IdFieldPair &f_obj, const wtt_app::IdFieldPair &s_obj) {
    SPDLOG_DEBUG("Linking m2m entries");

    // Check link-entries for given entries
    int links_count = MT::filter(
        f_obj.id_field_name + " = " + std::to_string(f_obj.id_value)
        + " AND " + s_obj.id_field_name + " = " + std::to_string(s_obj.id_value)
    ).size();

    // Link
    if (!FLT::exists(f_obj.id_value) || !SLT::exists(s_obj.id_value)) {  // Handle linking entries that don't exist
        std::cout << "Trying to link entries that don't exist" << std::endl;
        SPDLOG_ERROR("Trying to link entries that don't exist");
        std::exit(EXIT_FAILURE);
    } else if (links_count > 0) {  // HAndle linking entries that are already linked
        std::cout << "Already linked." << std::endl;
        SPDLOG_WARN("Trying to linkl entries that are already linked");
        std::exit(EXIT_FAILURE);
    } else {  // Link entries
        MT({
            {f_obj.id_field_name, std::to_string(f_obj.id_value)},
            {s_obj.id_field_name, std::to_string(s_obj.id_value)}
        }).save();
    }
}


// Helper function to unlink entries in m2m relationship
template<class MT>
void wtt_app::mtm_unlink(const wtt_app::IdFieldPair &f_obj, const wtt_app::IdFieldPair &s_obj) {
    SPDLOG_DEBUG("Unlinking m2m entries");

    // Get link-entriers for given entries
    std::vector<std::shared_ptr<MT>> links = MT::filter(
        f_obj.id_field_name + " = " + std::to_string(f_obj.id_value)
        + " AND " + s_obj.id_field_name + " = " + std::to_string(s_obj.id_value)
    );

    // Unlink
    if (links.size() < 1) {  // Handle unlinking of unlinked entries
        std::cout << "Nothing to unlink" << std::endl;
        SPDLOG_WARN("Trying to unlink entries that are not linked");
        std::exit(EXIT_FAILURE);
    }

    // Delete all link-entries
    for (auto link : links) {
        link->del();
    }
}
