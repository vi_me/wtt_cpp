#include <filesystem>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "wtt_models/Session.hpp"
#include "conf.h"


using ::testing::AllOf;
using ::testing::HasSubstr;


class SessionMocker : public Session {
public:
    MOCK_METHOD(void, stop, ());
};


TEST(SessionClassTest, constructor_test) {
    Model::drop_table("sessions");
    Session::table_created = false;

    ASSERT_FALSE(Session::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions"));

    Session *session = new Session();

    ASSERT_TRUE(Session::get_table_created());
    ASSERT_TRUE(Model::table_exists("sessions"));

    delete session;

    ASSERT_TRUE(Session::get_table_created());
    ASSERT_TRUE(Model::table_exists("sessions"));
}

TEST(SessionClassTest, constructor_test_missing_table) {
    Model::drop_table("sessions");
    Session::table_created = true;

    ASSERT_TRUE(Session::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions"));

    Session *session = new Session();

    ASSERT_TRUE(Session::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions"));

    delete session;

    ASSERT_TRUE(Session::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions"));
    Session::table_created = false;
}

TEST(SessionClassTest, constructor_with_values_test) {
    Model::drop_table("sessions");
    Session::table_created = false;

    ASSERT_FALSE(Session::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions"));

    std::vector<SqlValPair> test_session_values = {
        {"paused", "1"}
    };
    Session *session = new Session(test_session_values);

    ASSERT_TRUE(Session::get_table_created());
    ASSERT_TRUE(Model::table_exists("sessions"));
    ASSERT_EQ(
        session->get_field(test_session_values[0].name)->value,
        test_session_values[0].value
    );

    delete session;

    ASSERT_TRUE(Session::get_table_created());
    ASSERT_TRUE(Model::table_exists("sessions"));
}

TEST(SessionClassTest, constructor_with_values_test_missing_table) {
    Model::drop_table("sessions");
    Session::table_created = true;

    ASSERT_TRUE(Session::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions"));

    std::vector<SqlValPair> test_session_values = {
        {"paused", "1"}
    };
    Session *session = new Session(test_session_values);

    ASSERT_TRUE(Session::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions"));
    ASSERT_EQ(
        session->get_field(test_session_values[0].name)->value,
        test_session_values[0].value
    );

    delete session;

    ASSERT_TRUE(Session::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions"));
    Session::table_created = false;
}

TEST(SessionClassTest, get_session_test) {
    Session *session1 = new Session();
    session1->save();

    std::shared_ptr<Session> session2 = Session::get(session1->get_field("id")->value);

    ASSERT_EQ(session1->get_field("id")->value, session2->get_field("id")->value);

    session1->del();
    delete session1;
}

TEST(SessionClassTest, get_session_extended_test) {
    Session *session1 = new Session();
    session1->save();

    long id = std::stol(session1->get_field("id")->value);
    std::shared_ptr<Session> session2 = Session::get(id);

    ASSERT_EQ(session1->get_field("id")->value, session2->get_field("id")->value);

    session1->del();
    delete session1;
}

TEST(SessionClassTest, filter_session_test) {
    // int pre_insert_count = Session::filter().size();
    std::vector<SqlValPair> test_session_values = {
        {"paused", "1"}, {"paused", "0"}, {"paused", "1"} 
    };
    std::vector<Session> sessions;
    for (auto val_pair : test_session_values) {
        Session s = Session(std::vector<SqlValPair>({val_pair}));
        s.save();
        sessions.push_back(s);
    }

    // ASSERT_EQ(Session::filter().size(), pre_insert_count + test_session_values.size());
    ASSERT_EQ(Session::filter().size(), test_session_values.size());
    ASSERT_EQ(Session::filter("paused=1").size(), 2);  // replace with count from test_session_values
    for (auto s : sessions) {
        s.del();
    }
}

TEST(SessionClassTest, exists_session_test) {
    Session *session = new Session();
    session->save();

    ASSERT_TRUE(Session::exists(std::stol(session->get_field("id")->value)));
    session->del();
    delete session;
}

TEST(SessionClassTest, exists_session_test_false) {
    ASSERT_FALSE(Session::exists(666));
}

TEST(SessionClassTest, exists_session_extended_test) {
    Session *session = new Session();
    session->save();

    ASSERT_TRUE(Session::exists(session->get_field("id")->value));
    session->del();
    delete session;
}

TEST(SessionClassTest, exists_session_extended_test_false) {
    ASSERT_FALSE(Session::exists("666"));
}


TEST(SessionClassTest, set_value_test) {
    Session *session = new Session();
    ASSERT_EQ(session->get_field("paused")->value, "");
    session->set_value("paused", "1");
    ASSERT_EQ(session->get_field("paused")->value, "1");
    delete session;
}

TEST(SessionClassTest, set_value_failure_test) {
    Session *session = new Session();
    std::string f_name = "impossible_field_name";
    try {
        session->set_value(f_name, "1");
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Field \"" + f_name + "\" not found in Session model"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    delete session;
}

TEST(SessionClassTest, set_value_extended_test) {
    Session *session = new Session();
    ASSERT_EQ(session->get_field("paused")->value, "");
    session->set_value(SqlValPair({"paused", "1"}));
    ASSERT_EQ(session->get_field("paused")->value, "1");
    delete session; 
}

TEST(SessionClassTest, set_value_extended_failure_test) {
    Session *session = new Session();
    std::string f_name = "impossible_field_name";
    try {
        session->set_value(SqlValPair({f_name, "1"}));
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Field \"" + f_name + "\" not found in Session model"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    delete session;
}

TEST(SessionClassTest, get_field_test) {
    Session *session = new Session();
    ASSERT_EQ(session->get_field("paused")->value, "");
    session->set_value("paused", "1");
    ASSERT_EQ(session->get_field("paused")->value, "1");
    delete session;
}

TEST(SessionClassTest, get_field_failure_test) {
    Session *session = new Session();
    std::string f_name = "impossible_field_name";
    try {
        session->get_field(f_name);
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Field \"" + f_name + "\" not found in Session model"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    delete session;
}

TEST(SessionClassTest, get_fields_test) {
    Session *session = new Session();
    ASSERT_EQ(session->get_fields().size(), session->fields.size());
    for (int i = 0; i < session->fields.size(); i++) {
        ASSERT_EQ(session->get_fields()[i]->name, session->fields[i]->name);
        ASSERT_EQ(session->get_fields()[i]->value, session->fields[i]->value);
    }

    Session *session2 = new Session();
    for (int i = 0; i < session->fields.size(); i++) {
        ASSERT_EQ(session->get_fields()[i]->name, session2->get_fields()[i]->name);
    }
    delete session;
    delete session2;
}

TEST(SessionClassTest, get_constraints_test) {
    Session *session = new Session();
    ASSERT_EQ(session->get_constraints().size(), session->constraints.size());
    for (int i = 0; i < session->constraints.size(); i++) {
        ASSERT_EQ(session->get_constraints()[i], session->constraints[i]);
    }

    Session *session2 = new Session();
    for (int i = 0; i < session->constraints.size(); i++) {
        ASSERT_EQ(session->get_constraints()[i], session2->get_constraints()[i]);
    }
    delete session;
    delete session2;
}

TEST(SessionClassTest, start_session_test) {
    std::shared_ptr<Session> session = Session::start();
    std::vector<std::shared_ptr<Session>> last_created_session = Session::filter(
        "id=(SELECT MAX(id) FROM " + session->get_name() + ")"
    );

    ASSERT_EQ(session->get_field("active")->value, "1");
    ASSERT_EQ(
        last_created_session[0]->get_field("id")->value,
        session->get_field("id")->value
    );
    ASSERT_EQ(
        last_created_session[0]->get_field("start_timestamp")->value,
        session->get_field("start_timestamp")->value
    );
    session->del();
}

TEST(SessionClassTest, start_session_failure_test) {
    std::shared_ptr<Session> blocking_session = Session::start();
    try {
        std::shared_ptr<Session> session = Session::start();
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Active Session already exists"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    blocking_session->del();
}

TEST(SessionClassTest, start_at_session_test) {
    std::shared_ptr<Session> session = Session::start_at("1");
    std::vector<std::shared_ptr<Session>> last_created_session = Session::filter(
        "id=(SELECT MAX(id) FROM " + session->get_name() + ")"
    );

    ASSERT_EQ(session->get_field("active")->value, "1");
    ASSERT_EQ(
        last_created_session[0]->get_field("id")->value,
        session->get_field("id")->value
    );
    ASSERT_EQ(
        session->get_field("start_timestamp")->value,
        "1"
    );
    ASSERT_EQ(
        last_created_session[0]->get_field("start_timestamp")->value,
        session->get_field("start_timestamp")->value
    );
    session->del();
}

TEST(SessionClassTest, start_at_session_failure_test) {
    std::shared_ptr<Session> blocking_session = Session::start_at("1");
    try {
        std::shared_ptr<Session> session = Session::start_at("1");
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Active Session already exists"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    blocking_session->del();
}

TEST(SessionClassTest, stop_session_test) {
    std::shared_ptr<Session> session = Session::start();
    sleep(1);
    session->stop();
    std::vector<std::shared_ptr<Session>> last_created_session = Session::filter(
        "id=(SELECT MAX(id) FROM " + session->get_name() + ")"
    );

    ASSERT_EQ(session->get_field("active")->value, "0");
    ASSERT_EQ(
        last_created_session[0]->get_field("id")->value,
        session->get_field("id")->value
    );
    ASSERT_GT(
        std::stoi(session->get_field("end_timestamp")->value),
        0
    );
    ASSERT_GT(
        std::stoi(session->get_field("end_timestamp")->value),
        std::stoi(session->get_field("start_timestamp")->value)
    );
    session->del();
}

TEST(SessionClassTest, stop_session_failure_test) {
    std::shared_ptr<Session> session = Session::start();
    sleep(1);
    session->stop();
    try {
        session->stop();
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Stopping inactive Session"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    session->del();
}

TEST(SessionClassTest, stop_at_session_test) {
    std::shared_ptr<Session> session = Session::start();
    sleep(1);
    session->stop_at(std::to_string(std::chrono::duration_cast<std::chrono::seconds>(
        std::chrono::system_clock::now().time_since_epoch()
    ).count()));
    std::vector<std::shared_ptr<Session>> last_created_session = Session::filter(
        "id=(SELECT MAX(id) FROM " + session->get_name() + ")"
    );

    ASSERT_EQ(session->get_field("active")->value, "0");
    ASSERT_EQ(
        last_created_session[0]->get_field("id")->value,
        session->get_field("id")->value
    );
    ASSERT_GT(
        std::stoi(session->get_field("end_timestamp")->value),
        0
    );
    ASSERT_GT(
        std::stoi(session->get_field("end_timestamp")->value),
        std::stoi(session->get_field("start_timestamp")->value)
    );
    session->del();
}

TEST(SessionClassTest, stop_at_empty_value_session_test) {
    SessionMocker session;
    session.set_value("start_timestamp", "1");
    session.set_value("active", "1");
    session.save();

    EXPECT_CALL(session, stop());

    session.stop_at();

    session.del();
}

TEST(SessionClassTest, stop_at_session_failure_test) {
    std::shared_ptr<Session> session = Session::start();
    sleep(1);
    session->stop();
    try {
        session->stop_at();
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Stopping inactive session"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    session->del();
}

TEST(SessionClassTest, create_at_only_start_value_test) {
    std::shared_ptr<Session> session = Session::create_at("1");

    std::vector<std::shared_ptr<Session>> last_created_session = Session::filter(
        "id=(SELECT MAX(id) FROM " + session->get_name() + ")"
    );

    ASSERT_EQ(
        session->get_field("id")->value,
        last_created_session[0]->get_field("id")->value
    );
    ASSERT_THAT(
        session->get_field("start_timestamp")->value,
        AllOf(last_created_session[0]->get_field("start_timestamp")->value, "1")
    );

    session->del();
}

TEST(SessionClassTest, create_at_test) {
    std::shared_ptr<Session> session = Session::create_at("1", "2");

    std::vector<std::shared_ptr<Session>> last_created_session = Session::filter(
        "id=(SELECT MAX(id) FROM " + session->get_name() + ")"
    );

    ASSERT_EQ(
        session->get_field("id")->value,
        last_created_session[0]->get_field("id")->value
    );
    ASSERT_THAT(
        session->get_field("start_timestamp")->value,
        AllOf(last_created_session[0]->get_field("start_timestamp")->value, "1")
    );

    ASSERT_THAT(
        session->get_field("end_timestamp")->value,
        AllOf(last_created_session[0]->get_field("end_timestamp")->value, "2")
    );

    session->del();
}

TEST(SessionClassTest, create_at_session_db_connection_failure_test) {
    std::string new_db_name = std::string(DB) + ".lock";
    std::filesystem::copy(DB, new_db_name);
    std::filesystem::permissions(
        DB,
        std::filesystem::perms::owner_all | std::filesystem::perms::group_all,
        std::filesystem::perm_options::remove
    );

    std::ifstream fs;
    fs.open(DB, std::ios::in);

    if (!fs) {
        try {
            fs.close();
            std::shared_ptr<Session> session = Session::create_at("1", "2");
            FAIL() << "Expected runtime_error";
        } catch(const std::runtime_error &err) {
            EXPECT_THAT(err.what(), HasSubstr("ERROR opening DB"));
        } catch (...) {
            FAIL() << "Expected runtime_error";
        }
    } else {
        GTEST_SKIP() << "Test is running with elevated priviliges";
    }

    fs.close();
    std::filesystem::remove(DB);
    std::filesystem::rename(new_db_name, DB);
    Model::close_db();
}

TEST(SessionClassTest, create_at_session_db_insertion_failure_test) {
    std::shared_ptr<Session> session;
    try {
        session = session->create_at("2", "1");
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_THAT(err.what(), HasSubstr("Error while saving Model: CHECK constraint failed: sessions"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
}

TEST(SessionClassTest, get_active_session_no_session_test) {
    EXPECT_EQ(Session::get_active_session(), nullptr);
}

TEST(SessionClassTest, get_active_session_one_session_test) {
    std::shared_ptr<Session> session = Session::start();
    std::shared_ptr<Session> active_session = Session::get_active_session();

    EXPECT_EQ(active_session->get_field("active")->value, "1");
    EXPECT_EQ(
        active_session->get_field("id")->value,
        session->get_field("id")->value
    );
    session->del();
}

TEST(SessionClassTest, get_active_session_multiple_sessions_test) {
    std::shared_ptr<Session> session = Session::start();
    Session *session2 = new Session(std::vector<SqlValPair>{{"active", "1"}});
    session2->save();

    try {
        std::shared_ptr<Session> active_session = Session::get_active_session();
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Multiple active Sessions found"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }

    session->del();
    session2->del();
    delete session2;
}

TEST(SessionClassTest, pause_no_value_session_test) {
    std::shared_ptr<Session> session = Session::start();
    session->pause();
    std::vector<std::shared_ptr<Session>> last_created_session = Session::filter(
        "id=(SELECT MAX(id) FROM " + session->get_name() + ")"
    );

    EXPECT_EQ(last_created_session[0]->get_field("paused")->value, "1");
    EXPECT_EQ(
        last_created_session[0]->get_field("id")->value,
        session->get_field("id")->value
    );
    EXPECT_THAT(
        session->get_field("paused")->value,
        AllOf(last_created_session[0]->get_field("paused")->value, "1")
    );
    EXPECT_EQ(
        session->get_field("pause_start_timestamp")->value,
        last_created_session[0]->get_field("pause_start_timestamp")->value
    );
    session->del();
}

TEST(SessionClassTest, pause_passed_value_session_test) {
    std::shared_ptr<Session> session = Session::start();
    session->pause("1");
    std::shared_ptr<Session> last_created_session = Session::filter(
        "id=(SELECT MAX(id) FROM " + session->get_name() + ")"
    )[0];

    EXPECT_EQ(last_created_session->get_field("paused")->value, "1");
    EXPECT_EQ(
        last_created_session->get_field("id")->value,
        session->get_field("id")->value
    );
    EXPECT_THAT(
        session->get_field("paused")->value,
        AllOf(last_created_session->get_field("paused")->value, "1")
    );
    EXPECT_THAT(
        session->get_field("pause_start_timestamp")->value,
        AllOf(last_created_session->get_field("pause_start_timestamp")->value, "1")
    );
    session->del();
}

TEST(SessionClassTest, pause_session_failure_test) {
    Session *session = new Session(std::vector<SqlValPair>{{"paused", "1"}});
    session->save();

    try {
        session->pause();
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Session already paused"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }

    session->del();
    delete session;
}

TEST(SessionClassTest, unpause_no_value_session_test) {
    std::shared_ptr<Session> session = Session::start();
    session->pause();
    sleep(1);
    session->unpause();

    EXPECT_EQ(session->get_field("paused")->value, "0");
    EXPECT_EQ(
        std::stoi(session->get_field("pause")->value),
        1
    );
    session->del();
}

TEST(SessionClassTest, unpause_with_value_session_test) {
    std::shared_ptr<Session> session = Session::start();
    session->pause("1");
    session->unpause("2");

    EXPECT_EQ(session->get_field("paused")->value, "0");
    EXPECT_EQ(
        std::stoi(session->get_field("pause")->value),
        1
    );
    session->del();
}

TEST(SessionClassTest, unpause_session_failure_test) {
    std::shared_ptr<Session> session = Session::start();

    try {
        session->unpause();
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Can't unpause, Session not paused"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }

    session->del();
}

TEST(SessionClassTest, unpause_no_value_session_failure_test) {
    std::shared_ptr<Session> session = Session::start();
    session->pause(std::to_string(std::chrono::duration_cast<std::chrono::seconds>(
        std::chrono::system_clock::now().time_since_epoch()
    ).count() + 3600));

    try {
        session->unpause();
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Session pause duration can't be negative"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }

    session->del();
}

TEST(SessionClassTest, unpause_with_value_session_failure_test) {
    std::shared_ptr<Session> session = Session::start();
    session->pause();

    try {
        session->unpause("1");
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Session pause duration can't be negative"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }

    session->del();
}

TEST(SessionClassTest, print_session_test) {
    std::shared_ptr<Session> session = Session::start();
    sleep(1);
    session->stop();

    std::ostringstream out;
    out << *session;

    EXPECT_THAT(out.str(), HasSubstr("Session:"));
    EXPECT_THAT(out.str(), HasSubstr("Overall time:"));
    EXPECT_THAT(out.str(), HasSubstr("Work time:"));

    session->del();
}

TEST(SessionClassTest, print_active_session_test) {
    std::shared_ptr<Session> session = Session::start();

    std::ostringstream out;
    out << *session;

    EXPECT_THAT(out.str(), HasSubstr("Session:"));
    EXPECT_THAT(out.str(), HasSubstr("Overall time:"));
    EXPECT_THAT(out.str(), HasSubstr("Work time:"));

    session->del();
}

TEST(SessionClassTest, print_paused_session_test) {
    std::shared_ptr<Session> session = Session::start();
    session->pause();

    std::ostringstream out;
    out << *session;

    EXPECT_THAT(out.str(), HasSubstr("Session:"));
    EXPECT_THAT(out.str(), HasSubstr("Session paused"));
    EXPECT_THAT(out.str(), HasSubstr("Overall time:"));
    EXPECT_THAT(out.str(), HasSubstr("Work time:"));

    session->del();
}

TEST(SessionClassTest, print_previously_paused_session_test) {
    std::shared_ptr<Session> session = Session::start();
    session->get_field("pause")->set_value("50");
    session->update();

    std::ostringstream out;
    out << *session;

    EXPECT_THAT(out.str(), HasSubstr("Session:"));
    EXPECT_THAT(out.str(), HasSubstr("Overall time:"));
    EXPECT_THAT(out.str(), HasSubstr("Pause:"));
    EXPECT_THAT(out.str(), HasSubstr("Work time:"));

    session->del();
}
