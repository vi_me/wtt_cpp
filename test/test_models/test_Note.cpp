#include <string>
#include <vector>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "wtt_models/Session.hpp"
#include "wtt_models/Task.hpp"
#include "wtt_models/Note.hpp"


using ::testing::HasSubstr;


// TEST(NoteClassTest, constructor_test) {
//     Note *note = new Note();
//     ASSERT_TRUE(note->get_table_created());
//     delete note;
// }
// 
// TEST(NoteClassTest, constructor_with_values_test) {
//     std::vector<SqlValPair> test_note_values = {
//         {"text", "test"}
//     };
//     Note *note = new Note(test_note_values);
//     ASSERT_EQ(
//         note->get_field(test_note_values[0].name)->value,
//         test_note_values[0].value
//     );
//     delete note;
// }

TEST(NoteClassTest, constructor_test) {
    Model::drop_table("notes");
    Note::table_created = false;

    ASSERT_FALSE(Note::get_table_created());
    ASSERT_FALSE(Model::table_exists("notes"));

    Note *note = new Note();

    ASSERT_TRUE(Note::get_table_created());
    ASSERT_TRUE(Model::table_exists("notes"));

    delete note;

    ASSERT_TRUE(Note::get_table_created());
    ASSERT_TRUE(Model::table_exists("notes"));
}

TEST(NoteClassTest, constructor_test_missing_table) {
    Model::drop_table("notes");
    Note::table_created = true;

    ASSERT_TRUE(Note::get_table_created());
    ASSERT_FALSE(Model::table_exists("notes"));

    Note *note = new Note();

    ASSERT_TRUE(Note::get_table_created());
    ASSERT_FALSE(Model::table_exists("notes"));

    delete note;

    ASSERT_TRUE(Note::get_table_created());
    ASSERT_FALSE(Model::table_exists("notes"));
    Note::table_created = true;
}

TEST(NoteClassTest, constructor_with_values_test) {
    Model::drop_table("notes");
    Note::table_created = false;

    ASSERT_FALSE(Note::get_table_created());
    ASSERT_FALSE(Model::table_exists("notes"));

    std::vector<SqlValPair> test_note_values = {
        {"text", "test"}
    };
    Note *note = new Note(test_note_values);

    ASSERT_TRUE(Note::get_table_created());
    ASSERT_TRUE(Model::table_exists("notes"));
    ASSERT_EQ(
        note->get_field(test_note_values[0].name)->value,
        test_note_values[0].value
    );

    delete note;

    ASSERT_TRUE(Note::get_table_created());
    ASSERT_TRUE(Model::table_exists("notes"));
}

TEST(NoteClassTest, constructor_with_values_test_missing_table) {
    Model::drop_table("notes");
    Note::table_created = true;

    ASSERT_TRUE(Note::get_table_created());
    ASSERT_FALSE(Model::table_exists("notes"));

    std::vector<SqlValPair> test_note_values = {
        {"text", "test"}
    };
    Note *note = new Note(test_note_values);

    ASSERT_TRUE(Note::get_table_created());
    ASSERT_FALSE(Model::table_exists("notes"));
    ASSERT_EQ(
        note->get_field(test_note_values[0].name)->value,
        test_note_values[0].value
    );

    delete note;

    ASSERT_TRUE(Note::get_table_created());
    ASSERT_FALSE(Model::table_exists("notes"));
    Note::table_created = false;
}

TEST(NoteClassTest, get_note_test) {
    Note *note1 = new Note();
    note1->save();

    std::shared_ptr<Note> note2 = Note::get(note1->get_field("id")->value);

    ASSERT_EQ(note1->get_field("id")->value, note2->get_field("id")->value);

    note1->del();
    delete note1;
}

TEST(NoteClassTest, get_note_extended_test) {
    Note *note1 = new Note();
    note1->save();

    long id = std::stol(note1->get_field("id")->value);
    std::shared_ptr<Note> note2 = Note::get(id);

    ASSERT_EQ(note1->get_field("id")->value, note2->get_field("id")->value);

    note1->del();
    delete note1;
}

TEST(NoteClassTest, filter_note_test) {
    // int pre_insert_count = Note::filter().size();
    std::vector<SqlValPair> test_note_values = {
        {"text", "test"}, {"text", ""}, {"text", "test"} 
    };
    std::vector<Note> notes;
    for (auto val_pair : test_note_values) {
        Note n = Note(std::vector<SqlValPair>({val_pair}));
        n.save();
        notes.push_back(n);
    }

    // ASSERT_EQ(Note::filter().size(), pre_insert_count + test_note_values.size());
    ASSERT_EQ(Note::filter().size(), test_note_values.size());
    ASSERT_EQ(Note::filter("text=\"test\"").size(), 2);  // replace with count from test_note_values
    for (auto n : notes) {
        n.del();
    }
}

TEST(NoteClassTest, set_value_test) {
    Note *note = new Note();
    ASSERT_EQ(note->get_field("text")->value, "");
    note->set_value("text", "test");
    ASSERT_EQ(note->get_field("text")->value, "test");
    delete note;
}

TEST(NoteClassTest, set_value_failure_test) {
    Note *note = new Note();
    std::string f_name = "impossible_field_name";
    try {
        note->set_value(f_name, "test");
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Field \"" + f_name + "\" not found in Note model"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    delete note;
}

TEST(NoteClassTest, set_value_extended_test) {
    Note *note = new Note();
    ASSERT_EQ(note->get_field("text")->value, "");
    note->set_value(SqlValPair({"text", "test"}));
    ASSERT_EQ(note->get_field("text")->value, "test");
    delete note; 
}

TEST(NoteClassTest, set_value_extended_failure_test) {
    Note *note = new Note();
    std::string f_name = "impossible_field_name";
    try {
        note->set_value(SqlValPair({f_name, "test"}));
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Field \"" + f_name + "\" not found in Note model"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    delete note;
}

TEST(NoteClassTest, get_field_test) {
    Note *note = new Note();
    ASSERT_EQ(note->get_field("text")->value, "");
    note->set_value("text", "test");
    ASSERT_EQ(note->get_field("text")->value, "test");
    delete note;
}

TEST(NoteClassTest, get_field_failure_test) {
    Note *note = new Note();
    std::string f_name = "impossible_field_name";
    try {
        note->get_field(f_name);
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Field \"" + f_name + "\" not found in Note model"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    delete note;
}

TEST(NoteClassTest, get_fields_test) {
    Note *note = new Note();
    ASSERT_EQ(note->get_fields().size(), note->fields.size());
    for (int i = 0; i < note->fields.size(); i++) {
        ASSERT_EQ(note->get_fields()[i]->name, note->fields[i]->name);
        ASSERT_EQ(note->get_fields()[i]->value, note->fields[i]->value);
    }

    Note *note2 = new Note();
    for (int i = 0; i < note->fields.size(); i++) {
        ASSERT_EQ(note->get_fields()[i]->name, note2->get_fields()[i]->name);
    }
    delete note;
    delete note2;
}

TEST(NoteClassTest, get_constraints_test) {
    Note *note = new Note();
    ASSERT_EQ(note->get_constraints().size(), note->constraints.size());
    for (int i = 0; i < note->constraints.size(); i++) {
        ASSERT_EQ(note->get_constraints()[i], note->constraints[i]);
    }

    Note *note2 = new Note();
    for (int i = 0; i < note->constraints.size(); i++) {
        ASSERT_EQ(note->get_constraints()[i], note2->get_constraints()[i]);
    }
    delete note;
    delete note2;
}

TEST(NoteClassTest, print_note_test) {
    std::shared_ptr<Note> note = std::make_shared<Note>();
    note->save();

    std::ostringstream out;
    out << *note;

    EXPECT_THAT(out.str(), HasSubstr("ID:"));
    EXPECT_THAT(out.str(), Not(HasSubstr("Linked Session ID: ")));
    EXPECT_THAT(out.str(), Not(HasSubstr("Linked Task ID: ")));

    note->del();
}

TEST(NoteClassTest, print_note_linked_to_session_test) {
    std::shared_ptr<Session> session = Session::start();
    std::shared_ptr<Note> note = std::make_shared<Note>();
    note->get_field("session_id")->set_value(session->get_field("id")->value);
    note->save();

    std::ostringstream out;
    out << *note;

    EXPECT_THAT(out.str(), HasSubstr("ID: " + note->get_field("id")->value));
    EXPECT_THAT(out.str(), HasSubstr("Linked Session ID: " + session->get_field("id")->value));
    EXPECT_THAT(out.str(), Not(HasSubstr("Linked Task ID: ")));

    note->get_field("session_id")->set_value("NULL");
    note->save();

    std::ostringstream out2;
    out2 << *note;

    EXPECT_THAT(out2.str(), HasSubstr("ID: " + note->get_field("id")->value));
    EXPECT_THAT(out2.str(), Not(HasSubstr("Linked Session ID: ")));
    EXPECT_THAT(out2.str(), Not(HasSubstr("Linked Task ID: ")));

    session->del();
    note->del();
}

TEST(NoteClassTest, print_note_linked_to_task_test) {
    std::shared_ptr<Task> task = std::make_shared<Task>();
    task->save();
    std::shared_ptr<Note> note = std::make_shared<Note>();
    note->get_field("task_id")->set_value(task->get_field("id")->value);
    note->save();

    std::ostringstream out;
    out << *note;

    EXPECT_THAT(out.str(), HasSubstr("ID: " + note->get_field("id")->value));
    EXPECT_THAT(out.str(), Not(HasSubstr("Linked Session ID: ")));
    EXPECT_THAT(out.str(), HasSubstr("Linked Task ID: " + task->get_field("id")->value));

    note->get_field("task_id")->set_value("NULL");
    note->save();

    std::ostringstream out2;
    out2 << *note;

    EXPECT_THAT(out2.str(), HasSubstr("ID: " + note->get_field("id")->value));
    EXPECT_THAT(out2.str(), Not(HasSubstr("Linked Session ID: ")));
    EXPECT_THAT(out2.str(), Not(HasSubstr("Linked Task ID: ")));


    task->del();
    note->del();
}

TEST(NoteClassTest, print_note_linked_to_session_and_task_test) {
    std::shared_ptr<Session> session = Session::start();
    std::shared_ptr<Task> task = std::make_shared<Task>();
    task->save();
    std::shared_ptr<Note> note = std::make_shared<Note>();
    note->get_field("session_id")->set_value(session->get_field("id")->value);
    note->get_field("task_id")->set_value(task->get_field("id")->value);
    note->save();

    std::ostringstream out;
    out << *note;

    EXPECT_THAT(out.str(), HasSubstr("ID: " + note->get_field("id")->value));
    EXPECT_THAT(out.str(), HasSubstr("Linked Session ID: " + session->get_field("id")->value));
    EXPECT_THAT(out.str(), HasSubstr("Linked Task ID: " + task->get_field("id")->value));

    session->del();
    task->del();
    note->del();
}
