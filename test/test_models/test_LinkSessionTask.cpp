#include <string>
#include <vector>

#include "gtest/gtest.h"

#include "wtt_models/Task.hpp"
#include "wtt_models/Session.hpp"
#include "wtt_models/LinkSessionTask.hpp"


TEST(LinkSessionTaskClassTest, constructor_test) {
    Model::drop_table("sessions_tasks");
    LinkSessionTask::table_created = false;

    ASSERT_FALSE(LinkSessionTask::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions_tasks"));

    LinkSessionTask *link = new LinkSessionTask();

    ASSERT_TRUE(LinkSessionTask::get_table_created());
    ASSERT_TRUE(Model::table_exists("sessions_tasks"));

    delete link;

    ASSERT_TRUE(LinkSessionTask::get_table_created());
    ASSERT_TRUE(Model::table_exists("sessions_tasks"));
}

TEST(LinkSessionTaskClassTest, constructor_test_missing_table) {
    Model::drop_table("sessions_tasks");
    LinkSessionTask::table_created = true;

    ASSERT_TRUE(LinkSessionTask::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions_tasks"));

    LinkSessionTask *link = new LinkSessionTask();

    ASSERT_TRUE(LinkSessionTask::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions_tasks"));

    delete link;

    ASSERT_TRUE(LinkSessionTask::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions_tasks"));
    LinkSessionTask::table_created = true;
}

TEST(LinkSessionTaskClassTest, constructor_with_values_test) {
    Model::drop_table("sessions_tasks");
    LinkSessionTask::table_created = false;

    ASSERT_FALSE(LinkSessionTask::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions_tasks"));

    std::vector<SqlValPair> test_link_values = {
        {"task_id", "1"}, {"session_id", "1"}
    };
    LinkSessionTask *link = new LinkSessionTask(test_link_values);

    ASSERT_TRUE(LinkSessionTask::get_table_created());
    ASSERT_TRUE(Model::table_exists("sessions_tasks"));
    ASSERT_EQ(
        link->get_field(test_link_values[0].name)->value,
        test_link_values[0].value
    );

    delete link;

    ASSERT_TRUE(LinkSessionTask::get_table_created());
    ASSERT_TRUE(Model::table_exists("sessions_tasks"));
}

TEST(LinkSessionTaskClassTest, constructor_with_values_test_missing_table) {
    Model::drop_table("sessions_tasks");
    LinkSessionTask::table_created = true;

    ASSERT_TRUE(LinkSessionTask::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions_tasks"));

    std::vector<SqlValPair> test_link_values = {
        {"task_id", "1"}, {"session_id", "1"}
    };
    LinkSessionTask *link = new LinkSessionTask(test_link_values);

    ASSERT_TRUE(LinkSessionTask::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions_tasks"));
    ASSERT_EQ(
        link->get_field(test_link_values[0].name)->value,
        test_link_values[0].value
    );

    delete link;

    ASSERT_TRUE(LinkSessionTask::get_table_created());
    ASSERT_FALSE(Model::table_exists("sessions_tasks"));
    LinkSessionTask::table_created = false;
}

TEST(LinkSessionTaskClassTest, filter_link_session_task_test) {
    std::vector<SqlValPair> test_link_values = {
        {"task_id", "1"}, {"session_id", "1"},
        {"task_id", "2"}, {"session_id", "2"},
        {"task_id", "1"}, {"session_id", "3"},
    };
    std::vector<LinkSessionTask> links;
    for (int i = 0 ; i < test_link_values.size(); i+=2) {
        LinkSessionTask l = LinkSessionTask(std::vector<SqlValPair>(
            {test_link_values[i], test_link_values[i+1]}
        ));
        l.save();
        links.push_back(l);
    }

    ASSERT_EQ(LinkSessionTask::filter().size(), test_link_values.size()/2);
    ASSERT_EQ(LinkSessionTask::filter("task_id=1").size(), 2);
    for (auto l : links) {
        l.del();
    }
}

TEST(LinkSessionTaskClassTest, get_all_linked_to_session_test) {
    Session *session = new Session();
    session->save();
    std::vector<std::shared_ptr<Task>> tasks = LinkSessionTask::get_all_linked_to<Task>(
        "session", session->get_field("id")->value
    );

    ASSERT_EQ(tasks.size(), 0);

    Task *task1 = new Task();
    task1->save();
    Task *task2 = new Task();
    task2->save();
    Task *task3 = new Task();
    task3->save();

    LinkSessionTask *link1 = new LinkSessionTask({
        {"task_id", task1->get_field("id")->value},
        {"session_id", session->get_field("id")->value},
    });
    link1->save();
    LinkSessionTask *link2 = new LinkSessionTask({
        {"task_id", task2->get_field("id")->value},
        {"session_id", session->get_field("id")->value},
    });
    link2->save();

    tasks = LinkSessionTask::get_all_linked_to<Task>(
        "session", session->get_field("id")->value
    );

    ASSERT_EQ(tasks.size(), 2);

    session->del();
    delete session;
    task1->del();
    delete task1;
    task2->del();
    delete task2;
    task3->del();
    delete task3;
    link1->del();
    delete link1;
    link2->del();
    delete link2;
}

TEST(LinkSessionTaskClassTest, get_all_linked_to_task_test) {
    Task *task = new Task();
    task->save();
    std::vector<std::shared_ptr<Session>> sessions = LinkSessionTask::get_all_linked_to<Session>(
        "task", task->get_field("id")->value
    );

    ASSERT_EQ(sessions.size(), 0);

    Session *session1 = new Session();
    session1->save();
    Session *session2 = new Session();
    session2->save();
    Session *session3 = new Session();
    session3->save();

    LinkSessionTask *link1 = new LinkSessionTask({
        {"task_id", task->get_field("id")->value},
        {"session_id", session1->get_field("id")->value},
    });
    link1->save();
    LinkSessionTask *link2 = new LinkSessionTask({
        {"task_id", task->get_field("id")->value},
        {"session_id", session2->get_field("id")->value},
    });
    link2->save();

    sessions = LinkSessionTask::get_all_linked_to<Session>(
        "task", task->get_field("id")->value
    );

    ASSERT_EQ(sessions.size(), 2);

    task->del();
    delete task;
    session1->del();
    delete session1;
    session2->del();
    delete session2;
    session3->del();
    delete session3;
    link1->del();
    delete link1;
    link2->del();
    delete link2;
}

TEST(LinkSessionTaskClassTest, get_all_linked_to_failure_test) {
    try {
        std::vector<std::shared_ptr<Session>> sessions = LinkSessionTask::get_all_linked_to<Session>(
            "impossible_value", "1"
        );
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Selector is not part of Linking table"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }

}

TEST(LinkSessionTaskClassTest, get_all_linked_to_session_extended_test) {
    Session *session = new Session();
    session->save();
    std::vector<std::shared_ptr<Task>> tasks = LinkSessionTask::get_all_linked_to<Task>(
        "session", std::stol(session->get_field("id")->value)
    );

    ASSERT_EQ(tasks.size(), 0);

    Task *task1 = new Task();
    task1->save();
    Task *task2 = new Task();
    task2->save();
    Task *task3 = new Task();
    task3->save();

    LinkSessionTask *link1 = new LinkSessionTask({
        {"task_id", task1->get_field("id")->value},
        {"session_id", session->get_field("id")->value},
    });
    link1->save();
    LinkSessionTask *link2 = new LinkSessionTask({
        {"task_id", task2->get_field("id")->value},
        {"session_id", session->get_field("id")->value},
    });
    link2->save();

    tasks = LinkSessionTask::get_all_linked_to<Task>(
        "session", std::stol(session->get_field("id")->value)
    );

    ASSERT_EQ(tasks.size(), 2);

    session->del();
    delete session;
    task1->del();
    delete task1;
    task2->del();
    delete task2;
    task3->del();
    delete task3;
    link1->del();
    delete link1;
    link2->del();
    delete link2;
}

TEST(LinkSessionTaskClassTest, get_all_linked_to_task_extended_test) {
    Task *task = new Task();
    task->save();
    std::vector<std::shared_ptr<Session>> sessions = LinkSessionTask::get_all_linked_to<Session>(
        "task", std::stol(task->get_field("id")->value)
    );

    ASSERT_EQ(sessions.size(), 0);

    Session *session1 = new Session();
    session1->save();
    Session *session2 = new Session();
    session2->save();
    Session *session3 = new Session();
    session3->save();

    LinkSessionTask *link1 = new LinkSessionTask({
        {"task_id", task->get_field("id")->value},
        {"session_id", session1->get_field("id")->value},
    });
    link1->save();
    LinkSessionTask *link2 = new LinkSessionTask({
        {"task_id", task->get_field("id")->value},
        {"session_id", session2->get_field("id")->value},
    });
    link2->save();

    sessions = LinkSessionTask::get_all_linked_to<Session>(
        "task", std::stol(task->get_field("id")->value)
    );

    ASSERT_EQ(sessions.size(), 2);

    task->del();
    delete task;
    session1->del();
    delete session1;
    session2->del();
    delete session2;
    session3->del();
    delete session3;
    link1->del();
    delete link1;
    link2->del();
    delete link2;
}

TEST(LinkSessionTaskClassTest, get_all_linked_to_extended_failure_test) {
    try {
        std::vector<std::shared_ptr<Session>> sessions = LinkSessionTask::get_all_linked_to<Session>(
            "impossible_value", 1
        );
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Selector is not part of Linking table"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }

}

TEST(LinkSessionTaskClassTest, set_value_test) {
    LinkSessionTask *link = new LinkSessionTask();
    ASSERT_EQ(link->get_field("task_id")->value, "");
    link->set_value("task_id", "1");
    ASSERT_EQ(link->get_field("task_id")->value, "1");
    delete link;
}

TEST(LinkSessionTaskClassTest, set_value_failure_test) {
    LinkSessionTask *link = new LinkSessionTask();
    std::string f_name = "impossible_field_name";
    try {
        link->set_value(f_name, "test");
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Field \"" + f_name + "\" not found in Link for Session(s) and Task(s) model"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    delete link;
}

TEST(LinkSessionTaskClassTest, set_value_extended_test) {
    LinkSessionTask *link = new LinkSessionTask();
    ASSERT_EQ(link->get_field("session_id")->value, "");
    link->set_value(SqlValPair({"session_id", "1"}));
    ASSERT_EQ(link->get_field("session_id")->value, "1");
    delete link;
}

TEST(LinkSessionTaskClassTest, set_value_extended_failure_test) {
    LinkSessionTask *link = new LinkSessionTask();
    std::string f_name = "impossible_field_name";
    try {
        link->set_value(SqlValPair({f_name, "test"}));
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Field \"" + f_name + "\" not found in Link for Session(s) and Task(s) model"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    delete link;
}

TEST(LinkSessionTaskClassTest, get_field_test) {
    LinkSessionTask *link = new LinkSessionTask();
    ASSERT_EQ(link->get_field("task_id")->value, "");
    link->set_value("task_id", "1");
    ASSERT_EQ(link->get_field("task_id")->value, "1");
    delete link;
}

TEST(LinkSessionTaskClassTest, get_field_failure_test) {
    LinkSessionTask *link = new LinkSessionTask();
    std::string f_name = "impossible_field_name";
    try {
        link->get_field(f_name);
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Field \"" + f_name + "\" not found in Link for Session(s) and Task(s) model"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    delete link;
}

TEST(LinkSessionTaskClassTest, get_fields_test) {
    LinkSessionTask *link = new LinkSessionTask();
    ASSERT_EQ(link->get_fields().size(), link->fields.size());
    for (int i = 0; i < link->fields.size(); i++) {
        ASSERT_EQ(link->get_fields()[i]->name, link->fields[i]->name);
        ASSERT_EQ(link->get_fields()[i]->value, link->fields[i]->value);
    }

    LinkSessionTask *link2 = new LinkSessionTask();
    for (int i = 0; i < link->fields.size(); i++) {
        ASSERT_EQ(link->get_fields()[i]->name, link2->get_fields()[i]->name);
    }
    delete link;
    delete link2;
}

TEST(LinkSessionTaskClassTest, get_constraints_test) {
    LinkSessionTask *link = new LinkSessionTask();
    ASSERT_EQ(link->get_constraints().size(), link->constraints.size());
    for (int i = 0; i < link->constraints.size(); i++) {
        ASSERT_EQ(link->get_constraints()[i], link->constraints[i]);
    }

    LinkSessionTask *link2 = new LinkSessionTask();
    for (int i = 0; i < link->constraints.size(); i++) {
        ASSERT_EQ(link->get_constraints()[i], link2->get_constraints()[i]);
    }
    delete link;
    delete link2;
}
