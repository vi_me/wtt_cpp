#include <string>
#include <vector>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "wtt_models/LinkSessionTask.hpp"
#include "wtt_models/Session.hpp"
#include "wtt_models/Task.hpp"


using ::testing::HasSubstr;


TEST(TaskClassTest, constructor_test) {
    Model::drop_table("tasks");
    Task::table_created = false;

    ASSERT_FALSE(Task::get_table_created());
    ASSERT_FALSE(Model::table_exists("tasks"));

    Task *task = new Task();

    ASSERT_TRUE(Task::get_table_created());
    ASSERT_TRUE(Model::table_exists("tasks"));

    delete task;

    ASSERT_TRUE(Task::get_table_created());
    ASSERT_TRUE(Model::table_exists("tasks"));
}

TEST(TaskClassTest, constructor_test_missing_table) {
    Model::drop_table("tasks");
    Task::table_created = true;

    ASSERT_TRUE(Task::get_table_created());
    ASSERT_FALSE(Model::table_exists("tasks"));

    Task *task = new Task();

    ASSERT_TRUE(Task::get_table_created());
    ASSERT_FALSE(Model::table_exists("tasks"));

    delete task;

    ASSERT_TRUE(Task::get_table_created());
    ASSERT_FALSE(Model::table_exists("tasks"));
    Task::table_created = false;
}

TEST(TaskClassTest, constructor_with_values_test) {
    Model::drop_table("tasks");
    Task::table_created = false;

    ASSERT_FALSE(Task::get_table_created());
    ASSERT_FALSE(Model::table_exists("tasks"));

    std::vector<SqlValPair> test_task_values = {
        {"summary", "test"}
    };
    Task *task = new Task(test_task_values);

    ASSERT_TRUE(Task::get_table_created());
    ASSERT_TRUE(Model::table_exists("tasks"));
    ASSERT_EQ(
        task->get_field(test_task_values[0].name)->value,
        test_task_values[0].value
    );

    delete task;

    ASSERT_TRUE(Task::get_table_created());
    ASSERT_TRUE(Model::table_exists("tasks"));
}

TEST(TaskClassTest, constructor_with_values_test_missing_table) {
    Model::drop_table("tasks");
    Task::table_created = true;

    ASSERT_TRUE(Task::get_table_created());
    ASSERT_FALSE(Model::table_exists("tasks"));

    std::vector<SqlValPair> test_task_values = {
        {"summary", "test"}
    };
    Task *task = new Task(test_task_values);

    ASSERT_TRUE(Task::get_table_created());
    ASSERT_FALSE(Model::table_exists("tasks"));
    ASSERT_EQ(
        task->get_field(test_task_values[0].name)->value,
        test_task_values[0].value
    );

    delete task;

    ASSERT_TRUE(Task::get_table_created());
    ASSERT_FALSE(Model::table_exists("tasks"));
    Task::table_created = false;
}

TEST(TaskClassTest, get_task_test) {
    Task *task1 = new Task();
    task1->save();

    std::shared_ptr<Task> task2 = Task::get(task1->get_field("id")->value);

    ASSERT_EQ(task1->get_field("id")->value, task2->get_field("id")->value);

    task1->del();
    delete task1;
}

TEST(TaskClassTest, get_task_extended_test) {
    Task *task1 = new Task();
    task1->save();

    long id = std::stol(task1->get_field("id")->value);
    std::shared_ptr<Task> task2 = Task::get(id);

    ASSERT_EQ(task1->get_field("id")->value, task2->get_field("id")->value);

    task1->del();
    delete task1;
}

TEST(TaskClassTest, filter_task_test) {
    // int pre_insert_count = Task::filter().size();
    std::vector<SqlValPair> test_task_values = {
        {"summary", "test"}, {"summary", ""}, {"summary", "test"}
    };
    std::vector<Task> tasks;
    for (auto val_pair : test_task_values) {
        Task t = Task(std::vector<SqlValPair>({val_pair}));
        t.save();
        tasks.push_back(t);
    }

    // ASSERT_EQ(Task::filter().size(), pre_insert_count + test_task_values.size());
    ASSERT_EQ(Task::filter().size(), test_task_values.size());
    ASSERT_EQ(Task::filter("summary=\"test\"").size(), 2);  // replace with count from test_task_values
    for (auto n : tasks) {
        n.del();
    }
}

TEST(TaskClassTest, exists_task_test) {
    Task *task = new Task();
    task->save();

    ASSERT_TRUE(Task::exists(std::stol(task->get_field("id")->value)));
    task->del();
    delete task;
}

TEST(TaskClassTest, exists_task_test_false) {
    ASSERT_FALSE(Task::exists(666));
}

TEST(TaskClassTest, exists_task_extended_test) {
    Task *task = new Task();
    task->save();

    ASSERT_TRUE(Task::exists(task->get_field("id")->value));
    task->del();
    delete task;
}

TEST(TaskClassTest, exists_task_extended_test_false) {
    ASSERT_FALSE(Task::exists("666"));
}

TEST(TaskClassTest, set_value_test) {
    Task *task = new Task();
    ASSERT_EQ(task->get_field("summary")->value, "");
    task->set_value("summary", "test");
    ASSERT_EQ(task->get_field("summary")->value, "test");
    delete task;
}

TEST(TaskClassTest, set_value_failure_test) {
    Task *task = new Task();
    std::string f_name = "impossible_field_name";
    try {
        task->set_value(f_name, "test");
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Field \"" + f_name + "\" not found in Task model"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    delete task;
}

TEST(TaskClassTest, set_value_extended_test) {
    Task *task = new Task();
    ASSERT_EQ(task->get_field("summary")->value, "");
    task->set_value(SqlValPair({"summary", "test"}));
    ASSERT_EQ(task->get_field("summary")->value, "test");
    delete task;
}

TEST(TaskClassTest, set_value_extended_failure_test) {
    Task *task = new Task();
    std::string f_name = "impossible_field_name";
    try {
        task->set_value(SqlValPair({f_name, "test"}));
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Field \"" + f_name + "\" not found in Task model"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    delete task;
}

TEST(TaskClassTest, get_field_test) {
    Task *task = new Task();
    ASSERT_EQ(task->get_field("summary")->value, "");
    task->set_value("summary", "test");
    ASSERT_EQ(task->get_field("summary")->value, "test");
    delete task;
}

TEST(TaskClassTest, get_field_failure_test) {
    Task *task = new Task();
    std::string f_name = "impossible_field_name";
    try {
        task->get_field(f_name);
        FAIL() << "Expected runtime_error";
    } catch(const std::runtime_error &err) {
        EXPECT_EQ(err.what(), std::string("Field not found"));
    } catch (...) {
        FAIL() << "Expected runtime_error";
    }
    delete task;
}

TEST(TaskClassTest, get_fields_test) {
    Task *task = new Task();
    ASSERT_EQ(task->get_fields().size(), task->fields.size());
    for (int i = 0; i < task->fields.size(); i++) {
        ASSERT_EQ(task->get_fields()[i]->name, task->fields[i]->name);
        ASSERT_EQ(task->get_fields()[i]->value, task->fields[i]->value);
    }

    Task *task2 = new Task();
    for (int i = 0; i < task->fields.size(); i++) {
        ASSERT_EQ(task->get_fields()[i]->name, task2->get_fields()[i]->name);
    }
    delete task;
    delete task2;
}

TEST(TaskClassTest, get_constraints_test) {
    Task *task = new Task();
    ASSERT_EQ(task->get_constraints().size(), task->constraints.size());
    for (int i = 0; i < task->constraints.size(); i++) {
        ASSERT_EQ(task->get_constraints()[i], task->constraints[i]);
    }

    Task *task2 = new Task();
    for (int i = 0; i < task->constraints.size(); i++) {
        ASSERT_EQ(task->get_constraints()[i], task2->get_constraints()[i]);
    }
    delete task;
    delete task2;
}

TEST(TaskClassTest, print_task_test) {
    std::shared_ptr<Task> task = std::make_shared<Task>();
    task->save();

    std::ostringstream out;
    out << *task;

    EXPECT_THAT(out.str(), HasSubstr("ID:"));
    EXPECT_THAT(out.str(), HasSubstr("Status:"));
    EXPECT_THAT(out.str(), HasSubstr("Summary:"));

    task->del();
}

TEST(TaskClassTest, print_task_linked_to_session_test) {
    std::shared_ptr<Session> session = Session::start();
    session->save();
    std::shared_ptr<Task> task = std::make_shared<Task>();
    task->save();

    std::vector<SqlValPair> link_vps = {
        {"task_id",  task->get_field("id")->value},
        {"session_id",  session->get_field("id")->value}
    };
    std::shared_ptr<LinkSessionTask> link = std::make_shared<LinkSessionTask>(link_vps);
    link->save();

    std::ostringstream out;
    out << *task;

    EXPECT_THAT(out.str(), HasSubstr("ID: " + task->get_field("id")->value));
    EXPECT_THAT(out.str(), HasSubstr("Status:"));
    EXPECT_THAT(out.str(), HasSubstr("Summary:"));
    EXPECT_THAT(out.str(), HasSubstr("Linked Sessions : " + session->get_field("id")->value));

    session->del();
    task->del();
    link->del();
}

