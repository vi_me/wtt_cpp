#include <filesystem>

#include "spdlog/spdlog.h"
#include "spdlog/sinks/rotating_file_sink.h"
#include "sqlite3/sqlite3.h"
#include "gtest/gtest.h"
#include "wtt_utils/utils.hpp"
#include "conf.h"


int main(int argc, char **argv) {
    // wtt_utils::init_db();

    // TODO: wrap into utility func
    try {
        auto logger = spdlog::rotating_logger_mt(
            "basic_logger", LOGFILE, 1048576 * 5, 1
        );
        // auto logger = spdlog::default_logger();
        spdlog::set_default_logger(logger);
    }
    catch (const spdlog::spdlog_ex &ex) {
        std::cout << "Log initiation failed: " << ex.what() << std::endl;
    }

    spdlog::set_pattern(LOG_PATTERN);
    spdlog::set_level(LOG_LEVEL);

    ::testing::InitGoogleTest(&argc, argv);
    int test_res = RUN_ALL_TESTS();

    SPDLOG_DEBUG("Removing DB file used for testing.");
    // std::filesystem::remove(DB);

    return test_res;
}
