#include <string>
#include <type_traits>
#include "gtest/gtest.h"
#include "wtt_core/Field.hpp"


TEST(SqlValPairStructTest, constructor_test) {
    SqlValPair test_sqlvalpair = {"test_name", "test_value"};
    ASSERT_EQ(test_sqlvalpair.name, "test_name");
    ASSERT_EQ(test_sqlvalpair.value, "test_value");
}

TEST(FieldClassTest, construcor_test) {
    ASSERT_EQ(std::is_constructible_v<Field>, false);
    ASSERT_EQ(bool(std::is_constructible_v<Field, std::string>), false);
    ASSERT_EQ(bool(std::is_constructible_v<Field, std::string, std::string>), true);
    ASSERT_EQ(bool(std::is_constructible_v<Field, std::string, std::string, std::string>), true);
    ASSERT_EQ(bool(std::is_constructible_v<Field, Field>), true);
}

TEST(FieldClassTest, construcor_two_values_test) {
    Field *test_field = new Field("test_name", "test_sql_type");
    ASSERT_EQ(test_field->value, "");
    ASSERT_EQ(test_field->name, "test_name");
    ASSERT_EQ(test_field->sql_type, "test_sql_type");
    ASSERT_EQ(test_field->fk, "");
}

TEST(FieldClassTest, construcor_three_values_test) {
    Field *test_field = new Field("test_name", "test_sql_type", "test_foreign_key");
    ASSERT_EQ(test_field->value, "");
    ASSERT_EQ(test_field->name, "test_name");
    ASSERT_EQ(test_field->sql_type, "test_sql_type");
    ASSERT_EQ(test_field->fk, "test_foreign_key");
}

TEST(FieldClassTest, construcor_set_value_test) {
    Field *test_field = new Field("test_name", "test_sql_type");
    test_field->set_value("test_value");
    ASSERT_EQ(test_field->value, "test_value");
    ASSERT_EQ(test_field->name, "test_name");
    ASSERT_EQ(test_field->sql_type, "test_sql_type");
    ASSERT_EQ(test_field->fk, "");
}

TEST(FieldClassTest, construcor_copy_values_test) {
    Field *test_field_1 = new Field("test_name", "test_sql_type");
    Field *test_field_2 = new Field(*test_field_1);
    ASSERT_EQ(test_field_2->value, test_field_1->value);
    ASSERT_EQ(test_field_2->name, test_field_1->name);
    ASSERT_EQ(test_field_2->sql_type, test_field_1->sql_type);
    ASSERT_EQ(test_field_2->fk, test_field_1->fk);
    test_field_2->set_value("test_value");
    ASSERT_NE(test_field_2->value, test_field_1->value);
}
