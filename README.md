# wtt_cpp


## Usage

```sh
$ wtt --help-all
 __     __        ______      ______
/\ \  _ \ \      /\__  _\    /\__  _\
\ \ \/ ".\ \     \/_/\ \/    \/_/\ \/
 \ \__/".~\_\       \ \_\       \ \_\
  \/_/   \/_/        \/_/        \/_/

Work Time Tracker
Usage: bin/Release/wtt [OPTIONS] [SUBCOMMAND]

Options:
  -h,--help                   Print this help message and exit
  --help-all

Subcommands:
session
  W.T.T. Session
  Options:
    -l,--list-all Excludes: --notes --note-ids --start --end
                                Display all sessions
    -n,--notes Excludes: --list-all
                                List linked Notes
    --note-ids Excludes: --list-all
                                List linked Note IDs
    -s,--start TEXT Excludes: --list-all --end
                                Start timestamp
    -e,--end TEXT Excludes: --list-all --start
                                End Timestamp

start
  W.T.T. Session Start
  Options:
    -t,--timestamp TEXT         Start timestamp

end
  W.T.T. Session End
  Options:
    -t,--timestamp TEXT         Start timestamp

pause
  Add Pause to W.T.T. Session
  Options:
    -t,--timestamp TEXT         Pause start timestamp

unpause
  Unpause curretly paused W.T.T. Session
  Options:
    -t,--timestamp TEXT         Pause start timestamp

note
  W.T.T. Note
  Options:
    -i,--id INT Excludes: --list
                                Note ID
    -t,--text TEXT Excludes: --list --contents
                                Pass text for note
    -l,--list Excludes: --id --text --edit --session --unlink-session --unlink-task --unlink-all
                                List notes
    -c,--contents Excludes: --text --session --unlink-session --unlink-task --unlink-all
                                Show note text
    -e,--edit Needs: --id Excludes: --list
                                Edit existing note
    --session INT Excludes: --list --contents --unlink-session --unlink-task --unlink-all
                                Link note with session
    --unlink-session Needs: --id Excludes: --list --contents --session --task
                                Unlink note from session
    --task INT Excludes: --unlink-session --unlink-task --unlink-all
                                Link note with task
    --unlink-task Needs: --id Excludes: --list --contents --session --task
                                Unlink note from task
    -u,--unlink-all Needs: --id Excludes: --list --contents --session --task
                                Unlink task and session from note

task
  W.T.T. Task
  Options:
    -i,--id INT Excludes: --list
                                Task ID
    -l,--list Excludes: --id --edit --link-sessions --link-session --link-active-session --unlink-sessions --unlink-session
                                List tasks
    -n,--notes                  List linked Notes
    --note-ids                  List linked Note IDs
    -d,--description            Show Task Description
    -e,--edit Needs: --id Excludes: --list
                                Edit existing task
    -s,--status TEXT            Change status value of a Task
    --link-sessions INT ... Excludes: --list --unlink-sessions --unlink-session
                                Link Task with Sessionsos create a Task linked with Sessions
    --link-session INT Excludes: --list --unlink-sessions --unlink-session
                                Link Task with a Session or create a Task in a Session
    --link-active-session Excludes: --list --unlink-sessions --unlink-session
                                Link Task with an active Session or create a Task in an active Session
    --unlink-sessions INT ... Needs: --id Excludes: --list --link-sessions --link-session --link-active-session
                                Unlink Sessions from Task
    --unlink-session INT Needs: --id Excludes: --list --link-sessions --link-session --link-active-session
                                Unlink Session from task 
```


## Linking Sturcure

### 1

- Note can be linked to a single Task
- Task can be linked to multiple Notes

### 2

- Note can be linked to a single Session
- Session can be linked to multiple Notes

### 3

- Multiple Sessions can be linked to multiple Tasks
- Multiple Tasks can be linked to multiple Sessions

